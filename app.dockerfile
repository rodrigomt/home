FROM ubuntu:16.04

WORKDIR /
EXPOSE 8080
USER root

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update \
    && apt-get install unzip -y \
    && apt-get install default-jdk -y \
    && apt-get install wget -y \
    && apt-get update && apt-get install -y --no-install-recommends apt-utils

RUN wget https://downloads.apache.org/tomcat/tomcat-9/v9.0.34/bin/apache-tomcat-9.0.34.tar.gz \
    && tar -xvzf apache-tomcat-9.0.34.tar.gz \
    && mv apache-tomcat-9.0.34 /opt

RUN wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.48.zip \
    && unzip mysql-connector-java-5.1.48.zip \
    && mv mysql-connector-java-5.1.48/mysql-connector-java-5.1.48.jar /opt/apache-tomcat-9.0.34/lib \
    && rm -f mysql-connector-java-5.1.48.zip

RUN chmod +x /opt/apache-tomcat-9.0.34/bin/*.sh

COPY . /opt/apache-tomcat-9.0.34/webapps/
ADD ./config/server.xml /opt/apache-tomcat-9.0.34/conf/

CMD ["/opt/apache-tomcat-9.0.34/bin/catalina.sh", "run"]