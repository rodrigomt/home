package br.com.impactante.Home.Interface.Logout.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutController extends HttpServlet
{
    protected void doPost(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse)
    throws IOException, ServletException
    {
        httpServletResponse.setContentType("text/html");
        Cookie loginCookie = null;
        Cookie[] cookies = httpServletRequest.getCookies();
        if(cookies != null)
        {
            for (Cookie cookie : cookies)
            {
                if(cookie.getName().equals("HOME_SESSION"))
                {
                    loginCookie = cookie;
                    break;
                }
            }
        }

        if(loginCookie != null)
        {
            loginCookie.setMaxAge(0);
            httpServletResponse.addCookie(loginCookie);
        }

        httpServletResponse.sendRedirect("/login");

    }
}
