package br.com.impactante.Home.Interface.Ad.Controller;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import br.com.impactante.Home.Core.Controller.AbstractController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

public class AdsController extends HttpServlet
 {
    protected void doGet(
        HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse
    ) throws IOException, ServletException {
        String ad_id = httpServletRequest.getParameter("ad_id");
        AdApplication adApplication = AdApplicationFactory.create();

        RequestDispatcher view  = httpServletRequest.getRequestDispatcher("/ad/show.jsp");

        br.com.impactante.Home.Interface.Ad.View.Model.Ad ad              =  adApplication.getAdUnique(ad_id);
        br.com.impactante.Home.Interface.Ad.View.Model.Image image        = adApplication.getImage(ad_id);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Image> images = adApplication.getImageList(ad_id);

        httpServletRequest.setAttribute("images", images);
        httpServletRequest.setAttribute("ad", ad);
        httpServletRequest.setAttribute("image", image);

        view.include(httpServletRequest, httpServletResponse);
    }
}