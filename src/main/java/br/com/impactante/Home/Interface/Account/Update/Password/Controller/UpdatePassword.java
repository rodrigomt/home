package br.com.impactante.Home.Interface.Account.Update.Password.Controller;

import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Core.Controller.AbstractController;
import br.com.impactante.Home.Domain.Account.User;
import br.com.impactante.Home.Infrastructure.Authentication.HashUltis;
import br.com.impactante.Home.Infrastructure.Authentication.Session;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class UpdatePassword extends AbstractController
{
    protected void doPost(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse)
            throws ServletException, IOException
    {
        PrintWriter out                 = httpServletResponse.getWriter();
        List<String> passwords          = new ArrayList<String>();
        DiskFileItemFactory factory     = new DiskFileItemFactory();
        ServletFileUpload upload        = new ServletFileUpload(factory);
        UserApplication userApplication = UserApplicationFactory.create();


        try
        {
            List<FileItem> items = upload.parseRequest(httpServletRequest);

            for (FileItem item : items) {

                if (item.isFormField()) {

                    String field = item.getFieldName();
                    String value = item.getString();
                    passwords.add(value);

                }
            }

            String password = userApplication.getPassword(session.getCurrentUserId());
            String MD5 = HashUltis.getHashMd5(passwords.get(0));

            if(MD5.equals(password))
            {
                String newPassword = HashUltis.getHashMd5(passwords.get(1));
                userApplication.updatePassword(session.getCurrentUserId(), newPassword);
                out.print("Autentificado");
            }
            else
            {
                out.print("Autentificação falhou");
            }

        }
        catch (FileUploadException e){
            e.printStackTrace();
        }
    }
}
