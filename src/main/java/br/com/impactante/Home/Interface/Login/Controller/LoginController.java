package br.com.impactante.Home.Interface.Login.Controller;

import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Core.Controller.AbstractController;
import br.com.impactante.Home.Core.Controller.LoginAbstractController;
import br.com.impactante.Home.Infrastructure.Authentication.HashUltis;
import br.com.impactante.Home.Infrastructure.Authentication.Login;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginController extends LoginAbstractController
{
    protected  void doGet(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        RequestDispatcher view = httpServletRequest.getRequestDispatcher("/WEB-INF/login/login.jsp");
        view.include(httpServletRequest, httpServletResponse);
    }

    protected void doPost(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        String email                    = httpServletRequest.getParameter("email");
        String password                 = httpServletRequest.getParameter("password");
        UserApplication userApplication = UserApplicationFactory.create();
        String MD5 = HashUltis.getHashMd5(password);

        Login login                    = userApplication.login(email, MD5);

        if (login.emailIsValid()) {

            if (login.passwordIsValid()) {
                Cookie homeSessionCookie = new Cookie("HOME_SESSION", login.getUser().getId().toString());
                homeSessionCookie.setMaxAge(1500 * 60);
                httpServletResponse.addCookie(homeSessionCookie);
                httpServletResponse.sendRedirect("/");
            } else {
                RequestDispatcher requestDispatcher = getServletContext().
                        getRequestDispatcher("/WEB-INF/login/login.jsp");
                requestDispatcher.include(httpServletRequest, httpServletResponse);
            }

        } else {
            RequestDispatcher requestDispatcher = getServletContext().
                    getRequestDispatcher("/WEB-INF/login/login.jsp");
            requestDispatcher.include(httpServletRequest, httpServletResponse);
        }
    }
}