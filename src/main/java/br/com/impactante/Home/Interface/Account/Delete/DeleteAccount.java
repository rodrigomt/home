package br.com.impactante.Home.Interface.Account.Delete;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Core.Controller.AbstractController;
import br.com.impactante.Home.Infrastructure.Authentication.HashUltis;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class DeleteAccount extends AbstractController
{

    protected void doPost(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse)
            throws ServletException, IOException
    {
        PrintWriter out = httpServletResponse.getWriter();
        UserApplication userApplication = UserApplicationFactory.create();
        AdApplication adApplication = AdApplicationFactory.create();
        List<String> listAdId = adApplication.getAdUserId(session.getCurrentUserId());
        String password = userApplication.getPassword(session.getCurrentUserId());
        String passwordBd = null;
        DiskFileItemFactory factory     = new DiskFileItemFactory();
        ServletFileUpload upload        = new ServletFileUpload(factory);

        try
        {
            List<FileItem> items = upload.parseRequest(httpServletRequest);

            for (FileItem item : items) {

                if (item.isFormField()) {

                    String field = item.getFieldName();
                    String value = item.getString();
                    passwordBd = HashUltis.getHashMd5(value);

                }
            }

            if(passwordBd.equals(password))
            {
                for(int i = 0; i < listAdId.size(); i++)
                {
                    adApplication.deleteAd(listAdId.get(i));
                }

                userApplication.deleteUser(session.getCurrentUserId());

                httpServletResponse.setContentType("text/html");
                Cookie loginCookie = null;
                Cookie[] cookies = httpServletRequest.getCookies();
                if(cookies != null)
                {
                    for (Cookie cookie : cookies)
                    {
                        if(cookie.getName().equals("HOME_SESSION"))
                        {
                            loginCookie = cookie;
                            break;
                        }
                    }
                }

                if(loginCookie != null)
                {
                    loginCookie.setMaxAge(0);
                    httpServletResponse.addCookie(loginCookie);
                }
                out.print("/");

            }
            else
            {
                out.print("no pass");
            }
        }
        catch (FileUploadException e){
            e.printStackTrace();
        }





    }
}