package br.com.impactante.Home.Interface.Account.Update.Data.Controller;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Core.Controller.AbstractController;
import br.com.impactante.Home.Domain.Account.User;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.management.AttributeList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class EditDataAccountController extends AbstractController
{
    protected void doPost(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        PrintWriter out                 = httpServletResponse.getWriter();
        DiskFileItemFactory factory     = new DiskFileItemFactory();
        ServletFileUpload upload        = new ServletFileUpload(factory);
        UserApplication userApplication = UserApplicationFactory.create();


        try
        {
            List<FileItem> items = upload.parseRequest(httpServletRequest);

            String  name    = items.get(0).getString();
            String  email   = items.get(1).getString();
            String emailConfirm = "";
            List<User> emails = userApplication.getEmail();


            for(int i = 0; i < emails.size(); i++) {
                if(!emails.get(i).getId().equals(session.getCurrentUserId()))
                {
                    if(emails.get(i).getEmail().equals(email))
                    {
                        emailConfirm = emails.get(i).getEmail();
                    }
                }
            }

            if (emailConfirm.equals(email))
            {
                out.print("registrado");
            }

            if(emailConfirm.equals(""))
            {
                userApplication.updateUser(session.getCurrentUserId(), email, name);
                out.print("atualizado");

            }

        }
        catch (FileUploadException e){
            e.printStackTrace();
        }

    }
}