package br.com.impactante.Home.Interface.MyAccount.Controller;

import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Core.Controller.AbstractController;
import br.com.impactante.Home.Domain.Account.User;
import org.json.simple.JSONObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MyAccountController extends AbstractController
{
    protected void doGet(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse)
            throws ServletException, IOException
    {
        UserApplication userApplication  = UserApplicationFactory.create();
        User user                        = userApplication.getUser(session.getCurrentUserId());

        httpServletRequest.setAttribute("user", user);
        RequestDispatcher view = httpServletRequest.getRequestDispatcher("/WEB-INF/myAccount/myAccount.jsp");
        view.include(httpServletRequest, httpServletResponse);
    }

    protected void doPost(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse)
            throws ServletException, IOException
    {
        PrintWriter out                  = httpServletResponse.getWriter();
        UserApplication userApplication  = UserApplicationFactory.create();
        User user                        = userApplication.getUser(session.getCurrentUserId());

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("NAME", user.getName());
        jsonObject.put("EMAIL", user.getEmail());

        out.print(jsonObject);
    }
}
