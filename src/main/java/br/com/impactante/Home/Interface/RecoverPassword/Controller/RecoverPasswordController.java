package br.com.impactante.Home.Interface.RecoverPassword.Controller;

import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Core.Controller.LoginAbstractController;
import br.com.impactante.Home.Infrastructure.Authentication.Guid;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

public class RecoverPasswordController extends LoginAbstractController
{
    protected void doGet(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        RequestDispatcher view = httpServletRequest.getRequestDispatcher("/WEB-INF/recovery/recover-password.jsp");
        view.include(httpServletRequest, httpServletResponse);
    }

    protected void doPost(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        PrintWriter out                 = httpServletResponse.getWriter();
        UserApplication userApplication = UserApplicationFactory.create();
        String email                    = httpServletRequest.getParameter("email");
        String responseEmailDB          = userApplication.getEmailUser(email);

        if(responseEmailDB.equals(email)) {

            String idUser       = userApplication.getIdUser(responseEmailDB);
            String codeExistingStatus = userApplication.getCodeExisting(idUser);
            String id     = "";
            String code   = "";
            String status = "";
            String userId = "";

            if(codeExistingStatus.equals("") || codeExistingStatus.equals("0"))
            {
                id     = Guid.generatorId();
                code   = Guid.generatorCode();
                status = "1";
                userId = userApplication.getIdUser(email);

                userApplication.codeVerificationCreate(id, code, status, userId);

            } else {
                code = userApplication.getCodeNumber(idUser);
            }

            String to   = responseEmailDB;
            String from = "7d524fbccd-614984@inbox.mailtrap.io";

            final String username = "2ab0aafecbe2cd";
            final String password = "5fe2d464831e76";

            String host = "smtp.mailtrap.io";

            Properties properties = new Properties();
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", host);
            properties.put("mail.smtp.port", "2525");

            Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            try {
                Message message = new MimeMessage(session);

                message.setFrom(new InternetAddress(from));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
                message.setText("Code Verification : " + code);

                Transport.send(message);

                out.print("enviado");

            } catch (MessagingException e) {

                throw new RuntimeException(e);

            }


        } else {

            out.println("não cadastrado");

        }
    }
}
