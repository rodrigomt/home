package br.com.impactante.Home.Interface.Account.Update.Password.Controller;

import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Core.Controller.AbstractController;
import br.com.impactante.Home.Infrastructure.Authentication.HashUltis;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class NewPassword extends HttpServlet
{
    protected void doPost(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        PrintWriter out                  = httpServletResponse.getWriter();
        String code                      = httpServletRequest.getParameter("code");
        String password                  = httpServletRequest.getParameter("newPassword");
        UserApplication userApplication  = UserApplicationFactory.create();
        String id                        = userApplication.getUserIdCode(code);
        String newPassword               = HashUltis.getHashMd5(password);

        userApplication.updateNewPassword(id, newPassword);
        out.print("updated");

    }
}
