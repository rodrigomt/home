package br.com.impactante.Home.Interface.Ad.Controller;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class AdDeleteController extends HttpServlet
{
    public void doPost(HttpServletRequest httpServletRequest,
                       HttpServletResponse httpServletResponse)
            throws ServletException, IOException
    {
        PrintWriter out = httpServletResponse.getWriter();

        AdApplication adApplication = AdApplicationFactory.create();

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);

        List<FileItem> items = null;

        try {
            items = upload.parseRequest(httpServletRequest);

        } catch (FileUploadException e) {
            e.printStackTrace();
        }

        for (FileItem item : items) {

            if (item.isFormField()) {

                String field = item.getFieldName();
                String value = item.getString();
                adApplication.deleteAd(value);

            }
        }
    }
}
