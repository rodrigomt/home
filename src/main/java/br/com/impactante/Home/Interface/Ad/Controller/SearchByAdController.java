package br.com.impactante.Home.Interface.Ad.Controller;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import br.com.impactante.Home.Interface.Ad.View.Model.Ad;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SearchByAdController extends HttpServlet
{

    protected void doPost(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        PrintWriter out             = httpServletResponse.getWriter();
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload    = new ServletFileUpload(factory);
        JSONArray list              = new JSONArray();
        List<Ad> adUserList         = null;
        String tradingValue         = httpServletRequest.getParameter("trading");
        AdApplication adApplication = AdApplicationFactory.create();

        adUserList = adApplication.AdListTrading(tradingValue);

        for (Ad ad : adUserList)
        {
           JSONObject jsonObject = new JSONObject();
           jsonObject.put("ID_AD", ad.id);
           jsonObject.put("ADDRESS", ad.street);
           jsonObject.put("NUMBER", ad.number);
           jsonObject.put("ZONE", ad.zone);
           jsonObject.put("CEP", ad.cep);
           jsonObject.put("CITY", ad.city);
           jsonObject.put("STATE", ad.state);
           jsonObject.put("TYPE_IMMOBILE", ad.typeImmobile);
           jsonObject.put("SIZE", ad.size);
           jsonObject.put("BEDROOM", ad.bedroom);
           jsonObject.put("SUITE", ad.suite);
           jsonObject.put("BATHROOM", ad.bathroom);
           jsonObject.put("VACANCY", ad.vacancy);
           jsonObject.put("TRADING_TYPE", ad.tradingType);
           jsonObject.put("PRICE", ad.price);
           jsonObject.put("TITLE", ad.title);
           list.add(jsonObject);
        }

        out.print(list);
    }
}