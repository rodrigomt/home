package br.com.impactante.Home.Interface.RegisterAccount.Controller;
import java.io.IOException;
import java.io.PrintWriter;

import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Core.Controller.RegisterAbstractController;
import br.com.impactante.Home.Domain.Account.User;
import br.com.impactante.Home.Infrastructure.Authentication.Guid;
import br.com.impactante.Home.Infrastructure.Authentication.HashUltis;
import br.com.impactante.Home.Infrastructure.Authentication.Login;
import br.com.impactante.Home.Infrastructure.Persistence.Dao.UserDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegisterAccountController extends RegisterAbstractController
{

    protected void doGet(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        RequestDispatcher view = httpServletRequest.getRequestDispatcher("/WEB-INF/register/register.jsp");
        view.include(httpServletRequest, httpServletResponse);
    }

    protected void doPost(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        PrintWriter out = httpServletResponse.getWriter();

        String id       = httpServletRequest.getParameter("id");
        String name     = httpServletRequest.getParameter("name");
        String email    = httpServletRequest.getParameter("email");
        String password = httpServletRequest.getParameter("password");
        String typeId   = httpServletRequest.getParameter("type");
        String uri      = httpServletRequest.getParameter("uri");

        UserApplication userApplication = UserApplicationFactory.create();
        User userDate = userApplication.get(email);

        if(userDate.getEmail().equals(""))
        {
            String MD5 = HashUltis.getHashMd5(password);
            User user = new User(Guid.generatorId() , name, email, MD5);
            userApplication.registerAccount(user);

                Login login = userApplication.login(user.getEmail(), user.getPassword());
                if (login.emailIsValid()) {

                    if (login.passwordIsValid()) {
                        Cookie homeSessionCookie = new Cookie("HOME_SESSION", login.getUser().getId().toString());
                        homeSessionCookie.setMaxAge(30 * 60);
                        httpServletResponse.addCookie(homeSessionCookie);
                        httpServletResponse.sendRedirect("/");
                    } else {
                        RequestDispatcher requestDispatcher = getServletContext().
                                getRequestDispatcher("/WEB-INF/login/login.jsp");
                        requestDispatcher.include(httpServletRequest, httpServletResponse);
                    }

                } else {
                    RequestDispatcher requestDispatcher = getServletContext().
                            getRequestDispatcher("/WEB-INF/login/login.jsp");
                    requestDispatcher.include(httpServletRequest, httpServletResponse);
                }
        }
        else
        {
            out.println("O email informado já foi registrado !");
        }
    }
}