package br.com.impactante.Home.Interface.Ad.View.Model;

public class Image
{
    public String id;
    public String url;
    public String id_ad;

    public Image(
        String id,
        String url,
        String id_ad
    ){
        this.id     = id;
        this.url    = url;
        this.id_ad  = id_ad;
    }
}
