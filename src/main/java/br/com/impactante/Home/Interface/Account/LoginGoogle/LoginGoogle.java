package br.com.impactante.Home.Interface.Account.LoginGoogle;

import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Domain.Account.User;
import br.com.impactante.Home.Infrastructure.Authentication.Guid;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.util.Collections;

public class LoginGoogle extends HttpServlet
{
    protected void doPost(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        String token                    = httpServletRequest.getParameter("tokenId");
        String clientId                 = "142522972923-cokl1v3oucv6r5j7cfikapi9bpvrg9t0.apps.googleusercontent.com";
        PrintWriter out                 = httpServletResponse.getWriter();
        UserApplication userApplication = UserApplicationFactory.create();

        HttpTransport transport = new NetHttpTransport();
        JacksonFactory jsonFactory = JacksonFactory.getDefaultInstance();

        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                .setAudience(Collections.singletonList(clientId))
                .build();


        try {

            GoogleIdToken idToken = verifier.verify(token);

            if(idToken != null) {

                Payload payload         = idToken.getPayload();
                String userId           = payload.getSubject();
                String email            = payload.getEmail();
                boolean emailVerified   = Boolean.valueOf(payload.getEmailVerified());
                String name             = (String) payload.get("name");
                String pictureUrl       = (String) payload.get("picture");
                String locale           = (String) payload.get("locale");
                String familyName       = (String) payload.get("family_name");
                String givenName        = (String) payload.get("given_name");
                String registeredUser   = userApplication.getEmailUser(email);

                if(email.equals(registeredUser)) {

                    Cookie homeSessionCookie = new Cookie("HOME_SESSION", userApplication.getIdUser(email));
                    homeSessionCookie.setMaxAge(1500 * 60);
                    httpServletResponse.addCookie(homeSessionCookie);

                    out.print("validated");

                } else {

                    User user = new User(
                            Guid.generatorId(),
                            name,
                            email,
                            ""
                    );

                    userApplication.registerAccount(user);

                    Cookie homeSessionCookie = new Cookie("HOME_SESSION", userApplication.getIdUser(email));
                    homeSessionCookie.setMaxAge(1500 * 60);
                    httpServletResponse.addCookie(homeSessionCookie);

                    out.print("validated");

                }
            } else {
                out.print("Invalid ID Token.");
            }

        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

    }
}
