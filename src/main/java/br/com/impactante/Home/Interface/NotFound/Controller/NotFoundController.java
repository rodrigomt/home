package br.com.impactante.Home.Interface.NotFound.Controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NotFoundController extends HttpServlet
{
    protected void doGet(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        RequestDispatcher view = httpServletRequest.getRequestDispatcher("/WEB-INF/notFound/not-found.jsp");
        view.include(httpServletRequest, httpServletResponse);
    }
}