package br.com.impactante.Home.Interface.Ad.Controller;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import br.com.impactante.Home.Core.Controller.AbstractController;
import br.com.impactante.Home.Interface.Ad.View.Model.Ad;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.io.PrintWriter;
import java.util.List;

public class MyAdsController extends AbstractController
{
    protected void doGet(
        HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse
    ) throws IOException, ServletException
    {
        String id                   = session.getCurrentUserId();
        PrintWriter out             = httpServletResponse.getWriter();
        List<Ad> adUserList         = null;
        AdApplication adApplication = AdApplicationFactory.create();


        if(httpServletRequest.getParameter("trading") == null && httpServletRequest.getParameter("type") == null ||
           httpServletRequest.getParameter("trading").equals("2") && httpServletRequest.getParameter("type").equals("3"))
        {
            adUserList = adApplication.AdListUser(id);
        }
        else
        {
            String trading = httpServletRequest.getParameter("trading");
            String type    = httpServletRequest.getParameter("type");
            adUserList = adApplication.AdListFilter(trading, type, id);

        }

        httpServletRequest.setAttribute("list", adUserList);
        RequestDispatcher view = httpServletRequest.getRequestDispatcher("/WEB-INF/myAds/my-ads.jsp");
        view.include(httpServletRequest, httpServletResponse);
    }
}