package br.com.impactante.Home.Interface.Account.Code;

import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;
import br.com.impactante.Home.Core.Controller.LoginAbstractController;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class CodeVerification extends LoginAbstractController
{
    protected void doPost(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        String code                     = httpServletRequest.getParameter("code");
        PrintWriter out                 = httpServletResponse.getWriter();
        UserApplication userApplication = UserApplicationFactory.create();

        String codeDB = userApplication.getCodeVerification(code);

        if(codeDB.equals("1")) {
            userApplication.updateStatusCode("0", code);
            out.print("valid");
        } else {
            out.print("invalid");
        }
    }
}
