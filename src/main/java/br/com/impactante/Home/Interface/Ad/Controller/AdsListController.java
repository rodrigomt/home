package br.com.impactante.Home.Interface.Ad.Controller;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import br.com.impactante.Home.Pagination.Pagination;
import br.com.impactante.Home.Interface.Ad.View.Model.Ad;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class AdsListController extends HttpServlet
{
    protected void doGet(
        HttpServletRequest  httpServletRequest,
        HttpServletResponse httpServletResponse
    ) throws IOException, ServletException
    {
        String order                    = httpServletRequest.getParameter("order");
        String search                   = httpServletRequest.getParameter("search");
        String trading                  = httpServletRequest.getParameter("option");
        String page                     = httpServletRequest.getParameter("page");
        List<Ad> adList                 = null;
        int numberPages                 = 0;
        PrintWriter out                 = httpServletResponse.getWriter();
        AdApplication adApplication     = AdApplicationFactory.create();
        Pagination pagination = null;

        if(search.equals("") && trading.equals("0") || search.equals("") && trading.equals("1"))
        {
            if(order.equals("") || order.equals("0"))
            {
                pagination = paginationMount(adApplication.AdListTrading(trading), adApplication.getAdsPage(Integer.parseInt(page), trading), Integer.parseInt(page));
                adList = pagination.getIterator();
                numberPages = pagination.getNumberPages();
            }

            if(order.equals("1"))
            {
                pagination = paginationMount(adApplication.AdListTrading(trading), adApplication.AdListTradingOrderAsc(Integer.parseInt(page), trading), Integer.parseInt(page));
                adList = pagination.getIterator();
                numberPages = pagination.getNumberPages();
            }

            if(order.equals("2"))
            {
                pagination = paginationMount(adApplication.AdListTrading(trading), adApplication.AdListTradingOrderDesc(Integer.parseInt(page), trading), Integer.parseInt(page));
                adList = pagination.getIterator();
                numberPages = pagination.getNumberPages();
            }

        }

        if(!search.equals("") && trading.equals("0") || !search.equals("") && trading.equals("1"))
        {
            if(order.equals("") || order.equals("0")) {
                pagination = paginationMount(adApplication.getListAdSearch(search, trading), adApplication.getListAdSearchOffset(Integer.parseInt(page), search, trading), Integer.parseInt(page));
                adList = pagination.getIterator();
                numberPages = pagination.getNumberPages();
            }

            if(order.equals("1"))
            {
                pagination = paginationMount(adApplication.getListAdSearch(search, trading), adApplication.getListAdSearchOrderAsc(Integer.parseInt(page), search, trading), Integer.parseInt(page));
                adList = pagination.getIterator();
                numberPages = pagination.getNumberPages();
            }

            if(order.equals("2"))
            {
                pagination = paginationMount(adApplication.getListAdSearch(search, trading), adApplication.getListAdSearchOrderDesc(Integer.parseInt(page), search, trading), Integer.parseInt(page));
                adList = pagination.getIterator();
                numberPages = pagination.getNumberPages();
            }
        }

        if(search.equals("") && trading.equals(""))
        {
            if(order.equals("") || order.equals("0"))
            {
              pagination = paginationMount(adApplication.AdList(), adApplication.AdListOffset(Integer.parseInt(page)), Integer.parseInt(page));
              adList = pagination.getIterator();
              numberPages = pagination.getNumberPages();

            }

            if(order.equals("1"))
            {
                pagination = paginationMount(adApplication.AdList(), adApplication.AdListOrderAsc(Integer.parseInt(page)), Integer.parseInt(page));
                adList = pagination.getIterator();
                numberPages = pagination.getNumberPages();
            }

            if(order.equals("2"))
            {
                pagination = paginationMount(adApplication.AdList(), adApplication.AdListOrderDesc(Integer.parseInt(page)), Integer.parseInt(page));
                adList = pagination.getIterator();
                numberPages = pagination.getNumberPages();
            }
        }

        if(!pagination.isPageValid())
        {
            httpServletResponse.sendRedirect("/adsList?order=" + order + "&page=" + (Integer.parseInt(page) - 1) + "&search=" + search + "&option=" + trading);
        }


        RequestDispatcher view = httpServletRequest.getRequestDispatcher("/WEB-INF/ads/ads.jsp");
        httpServletRequest.setAttribute("list", adList);
        httpServletRequest.setAttribute("numberPages", numberPages);
        httpServletRequest.setAttribute("linkOne", "/adsList?order=" + order + "&page=");
        httpServletRequest.setAttribute("linkTwo", "&search=" + search + "&option=" + trading);
        httpServletRequest.setAttribute("page", Integer.parseInt(page));
        view.include(httpServletRequest,httpServletResponse);


    }

    private Pagination paginationMount(List<Ad> allAds, List<Ad> pageAds, int page)
    {
        Pagination pagination = new Pagination(allAds, pageAds, page);

        return pagination;
    }
}