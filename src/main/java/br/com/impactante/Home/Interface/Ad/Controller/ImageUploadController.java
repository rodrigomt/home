package br.com.impactante.Home.Interface.Ad.Controller;

import br.com.impactante.Home.Core.Controller.AbstractController;
import br.com.impactante.Home.Infrastructure.Authentication.Guid;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImageUploadController extends AbstractController
{
    protected void doPost(HttpServletRequest  httpServletRequest,
                          HttpServletResponse httpServletResponse) throws ServletException, IOException
    {
        PrintWriter out = httpServletResponse.getWriter();
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);

        try {

            List<FileItem> items = upload.parseRequest(httpServletRequest);
            FileItem item        =  items.get(0);

            if (!item.isFormField()) {
                String fileName = item.getName();
                String contentType = item.getContentType();

                File uploadDir = new File("/opt/apache-tomcat-9.0.31/webapps/src/main/webapp/img/");

                File file = new File(uploadDir, fileName);
                File fileNewName = new File(uploadDir, session.getNewName() + "." + contentType.substring(6));
                file.renameTo(fileNewName);

                try {
                    item.write(fileNewName);
                    out.print(fileNewName.getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (FileUploadException e) {
            e.printStackTrace();
        }
    }
}