package br.com.impactante.Home.Interface.Ad.Controller;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import br.com.impactante.Home.Core.Controller.AbstractController;
import br.com.impactante.Home.Domain.Ad.Ad;
import br.com.impactante.Home.Infrastructure.Authentication.Guid;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class NewAdController extends AbstractController
{
    protected void doGet(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse) throws ServletException, IOException
    {
        RequestDispatcher view = httpServletRequest.getRequestDispatcher("/WEB-INF/newAd/form-new-ad.jsp");
        view.include(httpServletRequest, httpServletResponse);
    }

    protected void doPost(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse) throws ServletException, IOException
    {
        PrintWriter out = httpServletResponse.getWriter();

        AdApplication adApplication = AdApplicationFactory.create();
        List<String> adDate = new ArrayList<String>();
        String userId = session.getCurrentUserId();
        File directory = new File("/opt/apache-tomcat-9.0.30/webapps/src/main/webapp/img/");
        String dir = directory.getAbsolutePath();


        Calendar calendar = Calendar.getInstance();
        calendar.getTime();


        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);

        List<FileItem> items = null;

        try {
            items = upload.parseRequest(httpServletRequest);
        } catch (FileUploadException e) {
            e.printStackTrace();
        }

        for (FileItem item : items) {

            if (item.isFormField()) {

                String field = item.getFieldName();
                String value = item.getString();
                adDate.add(value);

            }
        }

        Ad ad = new Ad(
                Guid.generatorId(),
                adDate.get(0),
                adDate.get(1),
                adDate.get(2),
                adDate.get(3),
                adDate.get(4),
                adDate.get(5),
                adDate.get(6),
                adDate.get(7),
                adDate.get(8),
                adDate.get(9),
                adDate.get(10),
                adDate.get(11),
                adDate.get(12),
                adDate.get(13),
                adDate.get(14),
                adDate.get(15),
                adDate.get(16),
                adDate.get(17),
                calendar,
                userId
        );

        adApplication.addAd(ad);

        for (int i = 18; i < adDate.size(); i++) {
            adApplication.addImage(Guid.generatorId(), "src/main/webapp/img/" + adDate.get(i).trim(), ad.getId());

        }

        httpServletResponse.sendRedirect("/myAds");
    }
}