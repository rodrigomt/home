package br.com.impactante.Home.Interface.Ad.Controller;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ImageDeleteController extends HttpServlet
{
    public void doPost(HttpServletRequest httpServletRequest,
                       HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        AdApplication adApplication = AdApplicationFactory.create();
        String imageName            = httpServletRequest.getParameter("image");
        String result               = "/opt/apache-tomcat-9.0.31/webapps/src/main/webapp/img/" + imageName;

        File file = new File(result);

        try {
            if(file.exists()) {
                file.delete();
                adApplication.deleteImage(imageName);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
