package br.com.impactante.Home.Interface.Ad.View.Model;

import br.com.impactante.Home.Domain.Ad.Image;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Ad
{
    public String id;
    public String street;
    public String number;
    public String zone;
    public String cep;
    public String city;
    public String state;
    public String typeImmobile;
    public String size;
    public String bedroom;
    public String suite;
    public String bathroom;
    public String vacancy;
    public String tradingType;
    public String price;
    public String condominium;
    public String iptu;
    public String title;
    public String description;
    public Calendar date;
    public String userId;
    public ArrayList<Image> imageDate;

    public Ad(
            String id,
            String street,
            String number,
            String zone,
            String cep,
            String city,
            String state,
            String typeImmobile,
            String size,
            String bedroom,
            String suite,
            String bathroom,
            String vacancy,
            String tradingType,
            String price,
            String condominium,
            String iptu,
            String title,
            String description,
            Calendar date,
            String userId,
            ArrayList<Image> imageDate
    ){
        this.id             = id;
        this.street         = street;
        this.number         = number;
        this.zone           = zone;
        this.cep            = cep;
        this.city           = city;
        this.state          = state;
        this.typeImmobile   = typeImmobile;
        this.size           = size;
        this.bedroom        = bedroom;
        this.suite          = suite;
        this.bathroom       = bathroom;
        this.vacancy        = vacancy;
        this.tradingType    = tradingType;
        this.price          = price;
        this.condominium    = condominium;
        this.iptu           = iptu;
        this.title          = title;
        this.description    = description;
        this.date           = date;
        this.userId         = userId;
        this.imageDate      = imageDate;
    }

}