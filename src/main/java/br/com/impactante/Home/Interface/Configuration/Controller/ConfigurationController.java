package br.com.impactante.Home.Interface.Configuration.Controller;

import br.com.impactante.Home.Core.Controller.AbstractController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ConfigurationController extends AbstractController
{
    protected void doGet(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        RequestDispatcher view = httpServletRequest.getRequestDispatcher("/WEB-INF/configuration/configuration.jsp");
        view.include(httpServletRequest, httpServletResponse);
    }
}
