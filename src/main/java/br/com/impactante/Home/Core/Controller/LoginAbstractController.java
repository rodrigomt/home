package br.com.impactante.Home.Core.Controller;

import br.com.impactante.Home.Infrastructure.Authentication.Session;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class LoginAbstractController extends HttpServlet
{
    Session session = new Session();

    @Override
    protected void service(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse)
    throws ServletException, IOException
    {
        if(session.IsValid(httpServletRequest.getCookies()))
        {
            httpServletResponse.sendRedirect("/");

        }

        super.service(httpServletRequest, httpServletResponse);
    }
}