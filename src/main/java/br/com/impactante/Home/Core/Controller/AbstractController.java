package br.com.impactante.Home.Core.Controller;

import br.com.impactante.Home.Infrastructure.Authentication.Session;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractController extends HttpServlet
{
    protected Session session;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        session = new Session();

        if (session.IsValid(req.getCookies())) {

            if (req.getRequestURI().equals("/login") || req.getRequestURI().equals("/register")) {
                resp.sendRedirect("/home");
            }

        } else {

            if (!req.getRequestURI().equals("/login")) {
                resp.sendRedirect("/login");
            }

        }

        super.service(req, resp);
    }
}
