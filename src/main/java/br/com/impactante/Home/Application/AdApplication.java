package br.com.impactante.Home.Application;

import br.com.impactante.Home.Domain.Ad.Ad;
import br.com.impactante.Home.Domain.Ad.AdRepositoryInterface;
import br.com.impactante.Home.Domain.Ad.Image;

import java.util.ArrayList;
import java.util.List;

public class AdApplication implements AdApplicationInterface
{
    private AdRepositoryInterface adRepository;
    private ArrayList<br.com.impactante.Home.Domain.Ad.Image> images;
    private List<String> parameters;

    public AdApplication(
        AdRepositoryInterface adRepository
    ) {
        this.adRepository = adRepository;
    }


    public void addAd(Ad ad)
    {
        this.adRepository.addAd(ad);
    }

    public void updateAd(Ad ad)
    {
        this.adRepository.updateAd(ad);
    }

    public void addImage(String id, String url, String idAd)
    {
        this.adRepository.addImage(id, url, idAd);
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> AdListUser(String userId)
    {
        List<Ad> ads = this.adRepository.getListAdUser(userId);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adUserViewModel;

        adUserViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adUserViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adUserViewModel;
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> AdListTrading(String trading)
    {
        List<Ad> ads = this.adRepository.getListAdTrading(trading);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adUserViewModel;

        adUserViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adUserViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adUserViewModel;

    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> AdListTradingOrderAsc(int page, String trading)
    {
        List<Ad> ads = this.adRepository.getListAdTradingOrderAsc(page, trading);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adUserViewModel;

        adUserViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adUserViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adUserViewModel;

    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> AdListTradingOrderDesc(int page, String trading)
    {
        List<Ad> ads = this.adRepository.getListAdTradingOrderDesc(page, trading);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adUserViewModel;

        adUserViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adUserViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adUserViewModel;

    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> AdListFilter(String trading, String type, String id)
    {
        List<Ad> ads = null;

        if (!trading.equals("2") && type.equals("3")) {
            ads = this.adRepository.getListAdFilter(trading, null, id);
        }

        if (trading.equals("2") && !type.equals("3")) {
            ads = this.adRepository.getListAdFilter(null, type, id);
        }

        if(!trading.equals("2") && !type.equals("3")) {
            ads = this.adRepository.getListAdFilter(trading, type, id);
        }

        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adListFilterViewModel
                = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad:ads) {
            adListFilterViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adListFilterViewModel;

    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> AdList() {
        List<Ad> ads = this.adRepository.getListAd();
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adViewModel;

        adViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                ad.getId(),
                ad.getStreet(),
                ad.getNumber(),
                ad.getZone(),
                ad.getCep(),
                ad.getCity(),
                ad.getState(),
                ad.getTypeImmobile(),
                ad.getSize(),
                ad.getBedroom(),
                ad.getSuite(),
                ad.getBathroom(),
                ad.getVacancy(),
                ad.getTradingType(),
                ad.getPrice(),
                ad.getCondominium(),
                ad.getIptu(),
                ad.getTitle(),
                ad.getDescription(),
                ad.getDate(),
                ad.getUserId(),
                ad.getImages()
            ));
        }

        return adViewModel;
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> AdListOffset(int page) {
        List<Ad> ads = this.adRepository.getListAdOffset(page);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adViewModel;

        adViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adViewModel;
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> AdListOrderAsc(int page) {
        List<Ad> ads = this.adRepository.getListAdOrderAsc(page);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adViewModel;

        adViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                ad.getId(),
                ad.getStreet(),
                ad.getNumber(),
                ad.getZone(),
                ad.getCep(),
                ad.getCity(),
                ad.getState(),
                ad.getTypeImmobile(),
                ad.getSize(),
                ad.getBedroom(),
                ad.getSuite(),
                ad.getBathroom(),
                ad.getVacancy(),
                ad.getTradingType(),
                ad.getPrice(),
                ad.getCondominium(),
                ad.getIptu(),
                ad.getTitle(),
                ad.getDescription(),
                ad.getDate(),
                ad.getUserId(),
                ad.getImages()
            ));
        }

        return adViewModel;
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> AdListOrderDesc(int page) {
        List<Ad> ads = this.adRepository.getListAdOrderDesc(page);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adViewModel;

        adViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                ad.getId(),
                ad.getStreet(),
                ad.getNumber(),
                ad.getZone(),
                ad.getCep(),
                ad.getCity(),
                ad.getState(),
                ad.getTypeImmobile(),
                ad.getSize(),
                ad.getBedroom(),
                ad.getSuite(),
                ad.getBathroom(),
                ad.getVacancy(),
                ad.getTradingType(),
                ad.getPrice(),
                ad.getCondominium(),
                ad.getIptu(),
                ad.getTitle(),
                ad.getDescription(),
                ad.getDate(),
                ad.getUserId(),
                ad.getImages()
            ));
        }

        return adViewModel;
    }

    public br.com.impactante.Home.Interface.Ad.View.Model.Ad getAdUnique(String adId)
    {
       Ad ad = this.adRepository.getAd(adId);
       ArrayList<Image> image = ad.getImages();


       br.com.impactante.Home.Interface.Ad.View.Model.Ad adViewModel =
               new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                       ad.getId(),
                       ad.getStreet(),
                       ad.getNumber(),
                       ad.getZone(),
                       ad.getCep(),
                       ad.getCity(),
                       ad.getState(),
                       ad.getTypeImmobile(),
                       ad.getSize(),
                       ad.getBedroom(),
                       ad.getSuite(),
                       ad.getBathroom(),
                       ad.getVacancy(),
                       ad.getTradingType(),
                       ad.getPrice(),
                       ad.getCondominium(),
                       ad.getIptu(),
                       ad.getTitle(),
                       ad.getDescription(),
                       ad.getDate(),
                       ad.getUserId(),
                       image

               );

       images = ad.getImages();

       return adViewModel;
    }

    public List<String> getAdUserId(String userId)
    {
        List<String> listAdUserId = this.adRepository.getAdUserId(userId);

        return listAdUserId;
    }

    public void deleteImage(String name)
    {
        this.adRepository.deleteImage(name);
    }

    public void deleteAd(String id)
    {
        this.adRepository.deleteAd(id);
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> getListAdSearch(String search, String trading)
    {
        List<Ad> ads = this.adRepository.getListAdSearch(search, trading);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adUserViewModel;

        adUserViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adUserViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adUserViewModel;
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> getListAdSearchOffset(int page, String search, String trading)
    {
        List<Ad> ads = this.adRepository.getListAdSearchOffset(page, search, trading);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adUserViewModel;

        adUserViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adUserViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adUserViewModel;
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> getListAdSearchOrderAsc(int page, String search, String trading)
    {
        List<Ad> ads = this.adRepository.getListAdSearchOrderAsc(page, search, trading);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adUserViewModel;

        adUserViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adUserViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adUserViewModel;
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> getListAdSearchOrderDesc(int page, String search, String trading)
    {
        List<Ad> ads = this.adRepository.getListAdSearchOrderDesc(page, search, trading);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adUserViewModel;

        adUserViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adUserViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adUserViewModel;
    }

    public br.com.impactante.Home.Interface.Ad.View.Model.Image getImage(String id_ad)
    {
        Image image = this.adRepository.getImageAd(id_ad);

        br.com.impactante.Home.Interface.Ad.View.Model.Image images = new br.com.impactante.Home.Interface.Ad.View.Model.Image(
                image.getIdImage(),
                image.getUrl(),
                image.getId_ad()
        );

        return images;
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Image> getImageList(String id_ad)
    {
        List<Image> images = this.adRepository.getImagesAd(id_ad);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Image> imageViewModel;

        imageViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Image>();

        for (Image image : images)
        {
            imageViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Image(
                    image.getIdImage(),
                    image.getUrl(),
                    image.getId_ad()
            ));
        }

        return imageViewModel;
    }

    public List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> getAdsPage(int page, String trading)
     {
        List<Ad> ads = this.adRepository.getAdsPage(page, trading);
        List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> adViewModel;

        adViewModel = new ArrayList<br.com.impactante.Home.Interface.Ad.View.Model.Ad>();

        for (Ad ad: ads) {
            adViewModel.add(new br.com.impactante.Home.Interface.Ad.View.Model.Ad(
                    ad.getId(),
                    ad.getStreet(),
                    ad.getNumber(),
                    ad.getZone(),
                    ad.getCep(),
                    ad.getCity(),
                    ad.getState(),
                    ad.getTypeImmobile(),
                    ad.getSize(),
                    ad.getBedroom(),
                    ad.getSuite(),
                    ad.getBathroom(),
                    ad.getVacancy(),
                    ad.getTradingType(),
                    ad.getPrice(),
                    ad.getCondominium(),
                    ad.getIptu(),
                    ad.getTitle(),
                    ad.getDescription(),
                    ad.getDate(),
                    ad.getUserId(),
                    ad.getImages()
            ));
        }

        return adViewModel;
    }

}