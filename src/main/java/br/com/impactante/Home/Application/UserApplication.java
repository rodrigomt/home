package br.com.impactante.Home.Application;

import br.com.impactante.Home.Domain.Account.User;
import br.com.impactante.Home.Domain.Account.UserRepositoryInterface;
import br.com.impactante.Home.Infrastructure.Authentication.Login;

import java.util.List;

public class UserApplication implements UserApplicationInterface
{
    private UserRepositoryInterface userRepository;

    public UserApplication(UserRepositoryInterface userRepository)
    {
        this.userRepository = userRepository;
    }

    public Login login(String email, String password)
    {
        User user = this.userRepository.get(email);

        return new Login(user, email, password);
    }

    public  User  get(String email)
    {
        User user = this.userRepository.get(email);

        return user;
    }

    public User getUser(String id)
    {
        User user = this.userRepository.getUser(id);

        return user;
    }

    public List<User> getEmail()
    {
        return this.userRepository.getEmail();

    }

    public String getPassword(String id)
    {
        String password = this.userRepository.getPassword(id);

        return password;
    }

    public void registerAccount(User user)
    {
        this.userRepository.registerAccount(user);
    }

    public String getNameUserCurrent(String id)
    {
        return this.userRepository.getNameUser(id);
    }

    public String getEmailUser(String email)
    {
        return this.userRepository.getEmailUser(email);
    }

    public void updateUser(String id, String email, String name)
    {
        this.userRepository.updateUser(id, email, name);
    }

    public void updatePassword(String id, String password)
    {
        this.userRepository.updatePassword(id, password);
    }

    public void deleteUser(String id)
    {
        this.userRepository.deleteUser(id);
    }

    public String getIdUser(String email)
    {
        return this.userRepository.getIdUser(email);
    }

    public String getCodeVerification(String code)
    {
        return this.userRepository.getCode(code);
    }

    public void codeVerificationCreate(String id, String code, String status, String userId)
    {
        this.userRepository.codeVerification(id, code, status, userId);
    }

    public String getCodeExisting(String userId)
    {
        return this.userRepository.getCodeExisting(userId);
    }

    public String getCodeNumber(String userId){
        return this.userRepository.getCodeNumber(userId);
    }

    public void updateStatusCode(String status, String code)
    {
        this.userRepository.updateStatusCode(status, code);
    }

    public String getUserIdCode(String code){
        return this.userRepository.getUserIdCode(code);
    }

    public void updateNewPassword(String userId, String password)
    {
        this.userRepository.updateNewPassword(userId, password);
    }

}