package br.com.impactante.Home.Application;

import br.com.impactante.Home.Domain.Ad.AdRepositoryInterface;
import br.com.impactante.Home.Infrastructure.Persistence.Dao.AdDao;

final public class AdApplicationFactory
{
    public static AdApplication create()
    {
        AdRepositoryInterface adRepository = new AdDao();

        return new AdApplication(adRepository);
    }
}