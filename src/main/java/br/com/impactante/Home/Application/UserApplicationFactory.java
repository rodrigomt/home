package br.com.impactante.Home.Application;

import br.com.impactante.Home.Domain.Account.UserRepositoryInterface;
import br.com.impactante.Home.Infrastructure.Persistence.Dao.UserDao;

final public class UserApplicationFactory
{
    public static UserApplication create()
    {
        UserRepositoryInterface userRepository = new UserDao();

        return new UserApplication(userRepository);
    }
}
