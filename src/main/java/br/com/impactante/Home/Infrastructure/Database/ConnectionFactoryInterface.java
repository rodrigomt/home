package br.com.impactante.Home.Infrastructure.Database;

import java.sql.Connection;

public interface ConnectionFactoryInterface
{
    public Connection getConnection();
}