package br.com.impactante.Home.Infrastructure.Authentication;

import br.com.impactante.Home.Domain.Account.User;
import br.com.impactante.Home.Domain.Account.UserNullObject;

public class Login
{
    private User user;
    private String email;
    private String password;

    public Login(User user, String email, String password)
    {
        this.user     = user;
        this.email    = email;
        this.password = password;
    }

    public User getUser()
    {
        return user;
    }

    public boolean emailIsValid()
    {
        if (user instanceof UserNullObject) {
            return false;
        }

        return user.getEmail().equals(email);
    }

    public boolean passwordIsValid()
    {
        return user.getPassword().equals(password);
    }
}
