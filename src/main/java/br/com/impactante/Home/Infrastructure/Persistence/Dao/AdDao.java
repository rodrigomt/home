package br.com.impactante.Home.Infrastructure.Persistence.Dao;

import br.com.impactante.Home.Domain.Ad.*;
import br.com.impactante.Home.Domain.Ad.AdRepositoryInterface;
import br.com.impactante.Home.Infrastructure.Database.ConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdDao implements AdRepositoryInterface
{

    private Connection connection;

    public AdDao()
    {

        this.connection = new ConnectionFactory().getConnection();

    }

    public void addAd(Ad ad)
    {

        String sql = "insert into adverts" +
                "(id, street, number, zone, cep, city, state," +
                " type_immobile, size, bedroom, suite, bathroom," +
                " vacancy, trading_type, price, condominium, iptu, title, description, date, user_id)" +
                "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try
        {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,  ad.getId());
            statement.setString(2,  ad.getStreet());
            statement.setString(3,  ad.getNumber());
            statement.setString(4,  ad.getZone());
            statement.setString(5,  ad.getCep());
            statement.setString(6,  ad.getCity());
            statement.setString(7,  ad.getState());
            statement.setString(8,  ad.getTypeImmobile());
            statement.setString(9,  ad.getSize());
            statement.setString(10, ad.getBedroom());
            statement.setString(11, ad.getSuite());
            statement.setString(12, ad.getBathroom());
            statement.setString(13, ad.getVacancy());
            statement.setString(14, ad.getTradingType());
            statement.setString(15, ad.getPrice());
            statement.setString(16, ad.getCondominium());
            statement.setString(17, ad.getIptu());
            statement.setString(18, ad.getTitle());
            statement.setString(19, ad.getDescription());
            statement.setDate(20, new java.sql.Date(ad.getDate().getTimeInMillis()));
            statement.setString(21, ad.getUserId());
            statement.execute();
            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void updateAd(Ad ad)
    {
        String sql = "update adverts set street=?, number=?, zone=?, cep=?," +
                     " city=?, state=?, type_immobile=?, size=?, bedroom=?, " +
                     "suite=?, bathroom=?, vacancy=?, trading_type=?, price=?, " +
                     "condominium=?, iptu=?, title=?, description=?, date=?, user_id=? where id=?";

        try
        {
            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setString(1,  ad.getStreet());
            statement.setString(2,  ad.getNumber());
            statement.setString(3,  ad.getZone());
            statement.setString(4,  ad.getCep());
            statement.setString(5,  ad.getCity());
            statement.setString(6,  ad.getState());
            statement.setString(7,  ad.getTypeImmobile());
            statement.setString(8,  ad.getSize());
            statement.setString(9,  ad.getBedroom());
            statement.setString(10,  ad.getSuite());
            statement.setString(11, ad.getBathroom());
            statement.setString(12, ad.getVacancy());
            statement.setString(13, ad.getTradingType());
            statement.setString(14, ad.getPrice());
            statement.setString(15, ad.getCondominium());
            statement.setString(16, ad.getIptu());
            statement.setString(17, ad.getTitle());
            statement.setString(18, ad.getDescription());
            statement.setDate(19, new java.sql.Date(ad.getDate().getTimeInMillis()));
            statement.setString(20, ad.getUserId());
            statement.setString(21, ad.getId());

            statement.execute();
            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

    }

    public void deleteAd(String id)
    {
        try
        {
            PreparedStatement statement = connection.prepareStatement("delete from images where id_ad=?");
            statement.setString(1, id);
            statement.execute();

            statement = connection.prepareStatement("delete from adverts where id=?");
            statement.setString(1, id);
            statement.execute();



            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void addImage(String id, String url, String idAd)
    {

        String sql = "insert into images" +
                "(id, url, id_ad) values(?,?,?)";

        try
        {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, id);
            statement.setString(2, url);
            statement.setString(3, idAd);
            statement.execute();
            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void deleteImage(String name)
    {
        try
        {
            PreparedStatement statement = connection.prepareStatement("delete from images where url=?");
            statement.setString(1, "src/main/webapp/img/" + name.trim());

            statement.execute();
            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public Ad getAd(String adId)
    {

        try {
            PreparedStatement statement = this.connection.
                    prepareStatement("select * from adverts where id=? ");
            statement.setString(1, adId);
            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            Date date = resultSet.getDate("date");
            Calendar adDate = Calendar.getInstance();
            adDate.setTime(date);

            Ad ad = new Ad(
                    resultSet.getString("id"),
                    resultSet.getString("street"),
                    resultSet.getString("number"),
                    resultSet.getString("zone"),
                    resultSet.getString("cep"),
                    resultSet.getString("city"),
                    resultSet.getString("state"),
                    resultSet.getString("type_immobile"),
                    resultSet.getString("size"),
                    resultSet.getString("bedroom"),
                    resultSet.getString("suite"),
                    resultSet.getString("bathroom"),
                    resultSet.getString("vacancy"),
                    resultSet.getString("trading_type"),
                    resultSet.getString("price"),
                    resultSet.getString("condominium"),
                    resultSet.getString("iptu"),
                    resultSet.getString("title"),
                    resultSet.getString("description"),
                    adDate,
                    resultSet.getString("user_id")
            );

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=? ");
            statement.setString(1, adId);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                ad.addImage(new Image(
                        resultSet.getString("id"),
                        resultSet.getString("url"),
                        resultSet.getString("id_ad")
                ));
            }

            resultSet.close();
            statement.close();

            return ad;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<String> getAdUserId(String userId)
    {
        try
        {
            List<String> listAdId = new ArrayList<String>();
            PreparedStatement statement = this.connection.prepareStatement("select id from adverts where user_id=?");
            statement.setString(1, userId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
            {
                listAdId.add(resultSet.getString("id"));
            }

            resultSet.close();
            statement.close();

            return listAdId;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAd()
    {

        try {

            PreparedStatement statement;
            ResultSet resultSet;

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList ads
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            statement = this.connection.
                    prepareStatement("select * from adverts");
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                ads.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : ads) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }
            }

            resultSet.close();
            statement.close();

            return ads;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdOffset(int page)
    {

        try {

            int off = (page - 1) * 5 ;
            if(off < 0){
                off = off * 0;
            }

            PreparedStatement statement;
            ResultSet resultSet;

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList ads
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            statement = this.connection.
                    prepareStatement("select * from adverts LIMIT 5 " + "OFFSET " + off);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                ads.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : ads) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }
            }

            resultSet.close();
            statement.close();

            return ads;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdOrderAsc(int page)
    {
        try {

            int off = (page - 1) * 5 ;
            if(off < 0){
                off = off * 0;
            }

            PreparedStatement statement;
            ResultSet resultSet;

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList ads
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            statement = this.connection.
                    prepareStatement("select * from adverts order by adverts.price asc LIMIT 5 " + "OFFSET " + off);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                ads.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : ads) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }
            }

            resultSet.close();
            statement.close();

            return ads;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdOrderDesc(int page)
    {
        try {

            int off = (page - 1) * 5 ;
            if(off < 0){
                off = off * 0;
            }

            PreparedStatement statement;
            ResultSet resultSet;

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList ads
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            statement = this.connection.
                    prepareStatement("select * from adverts order by adverts.price desc LIMIT 5 " + "OFFSET " + off);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );
                ads.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : ads) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }
            }

            resultSet.close();
            statement.close();

            return ads;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdUser(String userId)
    {

        try {
            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList adsUser
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            PreparedStatement statement = this.connection.
                    prepareStatement("select * from adverts where user_id=?");
            statement.setString(1, userId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                adsUser.add(ad);

            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad adTwo : adsUser) {
                statement.setString(1, adTwo.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    adTwo.addImage(image);
                }
            }
            resultSet.close();
            statement.close();
            return adsUser;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdTrading(String trading)
    {
        try
        {
            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList adRent
                = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            PreparedStatement statement = this.connection.
                    prepareStatement("select * from adverts, images where trading_type=? and adverts.id = images.id_ad");
            statement.setString(1, trading);
            ResultSet resultSet = statement.executeQuery();


            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                if (!adRent.contains(ad)) {
                    adRent.add(ad);

                    if (resultSet.getString("id") == null ||
                            resultSet.getString("url") == null ||
                            resultSet.getString("id_ad") == null) {
                        String id = "";
                        String url = "";
                        String idAd = "";
                        Image images = new Image(
                                id,
                                url,
                                idAd
                        );
                        ad.addImage(images);
                    } else {
                        ad.addImage(new Image(

                                resultSet.getString("id"),
                                resultSet.getString("url"),
                                resultSet.getString("id_ad")
                        ));
                    }
                } else {
                    if (resultSet.getString("id") == null ||
                            resultSet.getString("url") == null ||
                            resultSet.getString("id_ad") == null) {
                        String id = "";
                        String url = "";
                        String idAd = "";
                        Image images = new Image(
                                id,
                                url,
                                idAd
                        );
                        ad.addImage(images);
                    } else {
                        ad.addImage(new Image(
                                resultSet.getString("id"),
                                resultSet.getString("url"),
                                resultSet.getString("id_ad")
                        ));
                    }
                }
            }

            resultSet.close();
            statement.close();
            return adRent;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdTradingOrderAsc(int page,String trading)
    {
        try
        {
            int off = (page - 1) * 5;
            if(off < 0){
                off = off * 0;
            }

            PreparedStatement statement;
            ResultSet resultSet;

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList adRent
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            statement = this.connection.prepareStatement("select * from adverts where adverts.trading_type=? order by adverts.price asc LIMIT 5 " + "OFFSET " + off);
            statement.setString(1, trading);
            resultSet = statement.executeQuery();


            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                adRent.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : adRent) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }
            }

            resultSet.close();
            statement.close();
            return adRent;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdTradingOrderDesc(int page, String trading) {
        try {

            int off = (page - 1) * 5;
            if(off < 0){
                off = off * 0;
            }

            PreparedStatement statement;
            ResultSet resultSet;

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList adRent
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            statement = this.connection.prepareStatement("select * from adverts where trading_type=? order by adverts.price desc LIMIT 5 " + "OFFSET " + off);
            statement.setString(1, trading);
            resultSet = statement.executeQuery();


            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                adRent.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : adRent) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }

            }
            resultSet.close();
            statement.close();
            return adRent;
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdFilter(String trading, String type, String id)
    {
        try {

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList filteredAds
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            PreparedStatement statement;
            ResultSet resultSet = null;


            if (trading == null && !(type == null))
            {
                statement = this.connection.
                        prepareStatement("select * from adverts where type_immobile=? and user_id=?");
                statement.setString(1, type);
                statement.setString(2, id);
                resultSet = statement.executeQuery();
            }

            if (!(trading == null) && type == null)
            {
                statement = this.connection.
                        prepareStatement("select * from adverts where trading_type=? and user_id=?");
                statement.setString(1, trading);
                statement.setString(2, id);
                resultSet = statement.executeQuery();
            }

            if (!(trading == null) && !(type == null))
            {
                statement = this.connection.
                        prepareStatement("select * from adverts where trading_type=? and type_immobile=? and user_id=?");
                statement.setString(1, trading);
                statement.setString(2, type);
                statement.setString(3, id);
                resultSet = statement.executeQuery();
            }

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                filteredAds.add(ad);

            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad adTwo : filteredAds) {
                statement.setString(1, adTwo.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    adTwo.addImage(image);
                }
            }
            resultSet.close();
            statement.close();

            return filteredAds;
        } catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdSearch(String search, String trading)
    {
        try
        {
            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList ads
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            PreparedStatement statement = this.connection.
                    prepareStatement("select * from adverts, images where adverts.id = images.id_ad and adverts.title=? and adverts.trading_type=?");
            statement.setString(1, search);
            statement.setString(2, trading);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                if (!ads.contains(ad)) {
                    ads.add(ad);

                    if(resultSet.getString("id")  == null ||
                            resultSet.getString("url") == null ||
                            resultSet.getString("id_ad") == null)
                    {
                        String id = "";
                        String url = "";
                        String idAd = "";
                        Image images = new Image(
                                id,
                                url,
                                idAd
                        );
                        ad.addImage(images);
                    }
                    else {
                        ad.addImage(new Image(

                                resultSet.getString("id"),
                                resultSet.getString("url"),
                                resultSet.getString("id_ad")
                        ));
                    }
                } else {
                    if(resultSet.getString("id")  == null ||
                            resultSet.getString("url") == null ||
                            resultSet.getString("id_ad") == null)
                    {
                        String id = "";
                        String url = "";
                        String idAd = "";
                        Image images = new Image(
                                id,
                                url,
                                idAd
                        );
                        ad.addImage(images);
                    }
                    else {
                        ad.addImage(new Image(
                                resultSet.getString("id"),
                                resultSet.getString("url"),
                                resultSet.getString("id_ad")
                        ));
                    }
                }
            }

            resultSet.close();
            statement.close();

            return ads;

        }

        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdSearchOffset(int page, String search, String trading)
    {
        try
        {
            int off = (page - 1) * 5 ;
            if(off < 0){
                off = off * 0;
            }

            PreparedStatement statement;
            ResultSet resultSet;

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList ads
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            statement = this.connection.prepareStatement("select * from adverts where adverts.title=? and adverts.trading_type=? LIMIT 5 " + "OFFSET " + off);
            statement.setString(1, search);
            statement.setString(2, trading);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );
                ads.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : ads) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }
            }

            resultSet.close();
            statement.close();

            return ads;

        }

        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdSearchOrderAsc(int page, String search, String trading)
    {
        try
        {
            int off = (page - 1) * 5 ;
            if(off < 0){
                off = off * 0;
            }

            PreparedStatement statement;
            ResultSet resultSet;

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList ads
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            statement = this.connection.
                    prepareStatement("select * from adverts where adverts.title=? and adverts.trading_type=? order by adverts.price asc LIMIT 5 " + "OFFSET " + off);
            statement.setString(1, search);
            statement.setString(2, trading);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                ads.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : ads) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }
            }

            resultSet.close();
            statement.close();

            return ads;

        }

        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getListAdSearchOrderDesc(int page, String search, String trading)
    {
        try
        {
            int off = (page - 1) * 5 ;
            if(off < 0){
                off = off * 0;
            }

            PreparedStatement statement;
            ResultSet resultSet;

            br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList ads
                    = new br.com.impactante.Home.Infrastructure.Persistence.Dao.ArrayList();
            statement = this.connection.prepareStatement("select * from adverts where adverts.title=? and adverts.trading_type=? order by adverts.price desc LIMIT 5 " + "OFFSET " + off);
            statement.setString(1, search);
            statement.setString(2, trading);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                ads.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : ads) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }
            }

            resultSet.close();
            statement.close();

            return ads;

        }

        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public Image getImageAd(String adId)
    {
        try
        {
            PreparedStatement statement = this.connection.
                    prepareStatement("select * from images where id_ad=? ");
            statement.setString(1, adId);
            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            Image image = new Image(
                    resultSet.getString("id"),
                    resultSet.getString("url"),
                    resultSet.getString("id_ad")
            );

            resultSet.close();
            statement.close();

            return image;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

    }

    public List<Image> getImagesAd(String adId)
    {
        try
        {
            ArrayList<Image> images = new ArrayList<Image>();
            PreparedStatement statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            statement.setString(1, adId);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {

                Image image = new Image(
                        resultSet.getString("id"),
                        resultSet.getString("url"),
                        resultSet.getString("id_ad")
                );

                images.add(image);
            }
            resultSet.close();
            statement.close();
            return images;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<Ad> getAdsPage(int page, String trading)
    {
        try
        {
            PreparedStatement statement;
            ResultSet resultSet;

            int off = (page - 1) * 5 ;
            if(off < 0){
                off = off * 0;
            }
            ArrayList<Ad> ads = new ArrayList<Ad>();
            statement = this.connection.prepareStatement("select * from adverts where trading_type=? LIMIT 5 " + "OFFSET " + off);
            statement.setString(1, trading);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Date date = resultSet.getDate("date");
                Calendar dateAd = Calendar.getInstance();
                dateAd.setTime(date);

                String currentId = resultSet.getString("id");

                Ad ad = new Ad(
                        currentId,
                        resultSet.getString("street"),
                        resultSet.getString("number"),
                        resultSet.getString("zone"),
                        resultSet.getString("cep"),
                        resultSet.getString("city"),
                        resultSet.getString("state"),
                        resultSet.getString("type_immobile"),
                        resultSet.getString("size"),
                        resultSet.getString("bedroom"),
                        resultSet.getString("suite"),
                        resultSet.getString("bathroom"),
                        resultSet.getString("vacancy"),
                        resultSet.getString("trading_type"),
                        resultSet.getString("price"),
                        resultSet.getString("condominium"),
                        resultSet.getString("iptu"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        dateAd,
                        resultSet.getString("user_id")
                );

                ads.add(ad);
            }

            statement = this.connection.
                    prepareStatement("select * from images where id_ad=?");
            for (Ad ad : ads) {
                statement.setString(1, ad.getId());
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    Image image = new Image(
                            resultSet.getString("id"),
                            resultSet.getString("url"),
                            resultSet.getString("id_ad")
                    );

                    ad.addImage(image);
                }
            }

            resultSet.close();
            statement.close();
            return ads;
        }
        catch (SQLException e)
        {
            throw  new RuntimeException(e);
        }
    }

}