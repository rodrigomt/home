package br.com.impactante.Home.Infrastructure.Persistence.Dao;

import br.com.impactante.Home.Infrastructure.Database.ConnectionFactory;
import br.com.impactante.Home.Domain.Account.User;
import br.com.impactante.Home.Domain.Account.UserNullObject;
import br.com.impactante.Home.Domain.Account.UserRepositoryInterface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements UserRepositoryInterface
{
    private Connection connection;

    public UserDao()
    {
        this.connection = new ConnectionFactory().getConnection();
    }

    public User save(User user)
    {
        return user;
    }

    public User get(String email)
    {
        User user;

        try
        {
            PreparedStatement statement = this.connection.prepareStatement("select * from users where email=?");
            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
            {
                user = new User(
                    resultSet.getString("id"),
                    resultSet.getString("name"),
                    resultSet.getString("email"),
                    resultSet.getString("password")
                );

                resultSet.close();

                statement.close();

                return user;
            }
            else
            {
                return new UserNullObject();
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public User getUser(String id)
    {
        User user;

        try
        {
            PreparedStatement statement = this.connection.prepareStatement("select * from users where id=?");
            statement.setString(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
            {
                user = new User(
                        resultSet.getString("id"),
                        resultSet.getString("name"),
                        resultSet.getString("email"),
                        resultSet.getString("password")
                );

                resultSet.close();

                statement.close();

                return user;
            }
            else
            {
                return new UserNullObject();
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<User> getEmail()
    {
        try
        {
            List<User> emails = new ArrayList<User>();
            PreparedStatement statement = this.connection.prepareStatement("select * from users");

            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next())
            {

               User user = new User(
                       resultSet.getString("id"),
                       resultSet.getString("name"),
                       resultSet.getString("email"),
                       resultSet.getString("password")
               );

                emails.add(user);
            }

            resultSet.close();
            statement.close();

            return emails;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public String getPassword(String id)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("select password from users where id=?");
            statement.setString(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
            {

                String password = resultSet.getString("password");

                resultSet.close();
                statement.close();

                return password;
            }
            else
            {
                String password = "";
                return password;
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void registerAccount(User user)
    {
        String sql = "insert into users(id, name, email, password) value(?,?,?,?);";

        try
        {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getId());
            statement.setString(2, user.getName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.execute();
            statement.close();
        }
        catch(SQLException e)
        {
            throw new RuntimeException(e);
        }

    }

    public String getEmailUser(String email)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("select email from users where email=?");
            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
            {

                String name = resultSet.getString("email");

                resultSet.close();
                statement.close();

                return name;
            }
            else
            {
                String name = "";
                return name;
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public String getNameUser(String id)
    {
        User user;

        try
        {
            PreparedStatement statement = this.connection.prepareStatement("select name from users where id=?");
            statement.setString(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
            {

                String name = resultSet.getString("name");

                resultSet.close();
                statement.close();

                return name;
            }
            else
            {
                String name = "";
                return name;
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void updateUser(String id, String email, String name)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("update users set name=?, email=? where id=?");
            statement.setString(1, name);
            statement.setString(2, email);
            statement.setString(3, id);

            statement.execute();
            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void updatePassword(String id, String password)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("update users set password=? where id=?");
            statement.setString(1, password);
            statement.setString(2, id);

            statement.execute();
            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void deleteUser(String id)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("delete from users where id=?");
            statement.setString(1, id);

            statement.execute();
            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public String getIdUser(String email)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("select id from users where email=?");
            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
            {

                String id = resultSet.getString("id");

                resultSet.close();
                statement.close();

                return id;
            }
            else
            {
                String name = "";
                return name;
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void codeVerification(String id, String code, String status, String userId) {

        String sql = "insert into code_recovery(id, code, status, id_user) value(?,?,?,?);";

        try
        {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, id);
            statement.setString(2, code);
            statement.setString(3, status);
            statement.setString(4, userId);
            statement.execute();
            statement.close();
        }
        catch(SQLException e)
        {
            throw new RuntimeException(e);
        }

    }

    public String getCode(String code)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("select status from code_recovery where code=?");
            statement.setString(1, code);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
            {

                String codeVerification = resultSet.getString("status");

                resultSet.close();
                statement.close();

                return codeVerification;
            }
            else
            {
                String name = "";
                return name;
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public String getCodeExisting(String userId)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("select status from code_recovery where id_user=?");
            statement.setString(1, userId);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
            {

                String codeVerification = resultSet.getString("status");

                resultSet.close();
                statement.close();

                return codeVerification;
            }
            else
            {
                String name = "";
                return name;
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public String getCodeNumber(String userId)
    {
        try {
            PreparedStatement statement = this.connection.prepareStatement("select code from code_recovery where id_user=?");
            statement.setString(1, userId);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                String codeVerification = resultSet.getString("code");

                resultSet.close();
                statement.close();

                return codeVerification;
            } else {
                String name = "";
                return name;
            }

        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void updateStatusCode(String status, String code)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("update code_recovery set status=? where code=?");
            statement.setString(1, status);
            statement.setString(2, code);

            statement.execute();
            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public String getUserIdCode(String code)
    {
        try {
            PreparedStatement statement = this.connection.prepareStatement("select id_user from code_recovery where code=?");
            statement.setString(1, code);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                String idUser = resultSet.getString("id_user");

                resultSet.close();
                statement.close();

                return idUser;
            } else {
                String name = "";
                return name;
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void updateNewPassword(String userId, String password)
    {
        try
        {
            PreparedStatement statement = this.connection.prepareStatement("update users set password=? where id=?");
            statement.setString(1, password);
            statement.setString(2, userId);

            statement.execute();
            statement.close();
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }
}