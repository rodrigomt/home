package br.com.impactante.Home.Infrastructure.Authentication;

import java.security.SecureRandom;

public class Guid
{
    final static private int size = 40;

    public static String generatorId()
    {
        SecureRandom random = new SecureRandom();
        byte bytes[]        = random.generateSeed(Guid.size);
        StringBuffer uid    = new StringBuffer();

        for (byte _byte: bytes) {
            uid.append(Math.abs(_byte));
        }

        return uid.substring(0, 39);
    }

    public static String generatorCode()
    {
        SecureRandom random = new SecureRandom();
        byte bytes[]        = random.generateSeed(Guid.size);
        StringBuffer uid    = new StringBuffer();

        for (byte _byte: bytes) {
            uid.append(Math.abs(_byte));
        }

        return uid.substring(0, 5);
    }

    public static String generatorPassword()
    {
        SecureRandom random = new SecureRandom();
        byte bytes[]        = random.generateSeed(Guid.size);
        StringBuffer uid    = new StringBuffer();

        for (byte _byte: bytes) {
            uid.append(Math.abs(_byte));
        }

        return uid.substring(0, 10);
    }
}
