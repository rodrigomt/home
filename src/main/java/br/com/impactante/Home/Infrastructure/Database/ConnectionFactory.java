package br.com.impactante.Home.Infrastructure.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory
{
    public Connection getConnection()
    {
        try
        {
            return DriverManager.getConnection("jdbc:mysql://db:3306/home", "home", "home456");
        }
        catch (SQLException exception)
        {
            throw new RuntimeException(exception);
        }
    }
}
