package br.com.impactante.Home.Infrastructure.Persistence.Dao;

import br.com.impactante.Home.Domain.Ad.Ad;

public class ArrayList extends java.util.ArrayList<Ad>
{
    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size(); i++)
            if (get(i).getId().equals(((Ad) o).getId()))
                return true;

        return false;
    }
}
