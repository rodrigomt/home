package br.com.impactante.Home.Infrastructure.Authentication;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Application.AdApplicationFactory;
import br.com.impactante.Home.Application.UserApplication;
import br.com.impactante.Home.Application.UserApplicationFactory;

import javax.servlet.http.Cookie;

public class Session
{
    private Cookie[] cookies;
    private String id;

    public Boolean IsValid(Cookie[] cookies)
    {
        if(cookies != null)
        {
            for (Cookie cookie : cookies)
            {
                if(cookie.getName().equals("HOME_SESSION"))
                {
                    id = cookie.getValue();
                    return true;
                }
            }
        }

        return false;
    }

    public String getCurrentUserId()
    {
        return id;
    }

    public String getCurrentUserName()
    {
        UserApplication userApplication = UserApplicationFactory.create();
        String name = userApplication.getNameUserCurrent(getCurrentUserId());

        return name;
    }

    public String getNewName()
    {
        return Guid.generatorId();
    }
}