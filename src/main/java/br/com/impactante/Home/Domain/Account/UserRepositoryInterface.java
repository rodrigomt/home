package br.com.impactante.Home.Domain.Account;

import java.util.List;

public interface UserRepositoryInterface
{
    public User save(User user);
    public User get(String email);
    public User getUser(String id);
    public List<User> getEmail();
    public String getPassword(String id);
    public void registerAccount(User user);
    public String getEmailUser(String email);
    public String getNameUser(String id);
    public void updateUser(String id, String email, String name);
    public void updatePassword(String id, String password);
    public void deleteUser(String id);
    public String getIdUser(String email);
    public void codeVerification(String id, String code, String status, String userId);
    public String getCode(String code);
    public String getCodeExisting(String userId);
    public String getCodeNumber(String userId);
    public void updateStatusCode(String status, String code);
    public String getUserIdCode(String code);
    public void updateNewPassword(String userId, String password);
}
