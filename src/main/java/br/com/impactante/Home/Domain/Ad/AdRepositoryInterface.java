package br.com.impactante.Home.Domain.Ad;

import java.util.List;

public interface AdRepositoryInterface
{
    public void addAd(Ad ad);
    public void updateAd(Ad ad);
    public void deleteAd(String id);

    public void addImage(String id, String url, String idAd);
    public void deleteImage(String name);

    public Ad getAd(String adId);
    public List<Ad> getListAd();
    public List<Ad> getListAdOffset(int page);
    public List<Ad> getListAdOrderAsc(int page);
    public List<Ad> getListAdOrderDesc(int page);

    public List<Ad> getListAdUser(String userId);
    public List<String> getAdUserId(String userId);
    public List<Ad> getListAdTrading(String trading);
    public List<Ad> getListAdTradingOrderAsc(int page, String trading);
    public List<Ad> getListAdTradingOrderDesc(int page, String trading);
    public List<Ad> getListAdFilter(String trading, String type, String id);

    public List<Ad> getListAdSearch(String search, String trading);
    public List<Ad> getListAdSearchOffset(int page, String search, String trading);
    public List<Ad> getListAdSearchOrderAsc(int page, String search, String trading);
    public List<Ad> getListAdSearchOrderDesc(int page, String search, String trading);

    public List<Image> getImagesAd(String adId);
    public Image getImageAd(String adId);

    public List<Ad> getAdsPage(int page, String trading);

}
