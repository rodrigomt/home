package br.com.impactante.Home.Domain.Ad;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Ad
{
    private String id;
    private String street;
    private String number;
    private String zone;
    private String cep;
    private String city;
    private String state;
    private String typeImmobile;
    private String size;
    private String bedroom;
    private String suite;
    private String bathroom;
    private String vacancy;
    private String tradingType;
    private String price;
    private String condominium;
    private String iptu;
    private String title;
    private String description;
    private Calendar date;
    private String userId;
    private ArrayList<Image> images;

    public Ad (
        String id,
        String street,
        String number,
        String zone,
        String cep,
        String city,
        String state,
        String typeImmobile,
        String size,
        String bedroom,
        String suite,
        String bathroom,
        String vacancy,
        String tradingType,
        String price,
        String condominium,
        String iptu,
        String title,
        String description,
        Calendar date,
        String userId
    ) {
        this.id           = id;
        this.street       = street;
        this.number       = number;
        this.zone         = zone;
        this.cep          = cep;
        this.city         = city;
        this.state        = state;
        this.typeImmobile = typeImmobile;
        this.size         = size;
        this.bedroom      = bedroom;
        this.suite        = suite;
        this.bathroom     = bathroom;
        this.vacancy      = vacancy;
        this.tradingType  = tradingType;
        this.price        = price;
        this.condominium  = condominium;
        this.iptu         = iptu;
        this.title        = title;
        this.description  = description;
        this.date         = date;
        this.userId      = userId;
        this.images       = new ArrayList<Image>();
    }

    public String getId()
    {
        return id;
    }

    public String getStreet()
    {
        return street;
    }

    public String getNumber()
    {
        return number;
    }

    public String getZone()
    {
        return zone;
    }

    public String getCep()
    {
        return cep;
    }

    public String getCity()
    {
        return city;
    }

    public String getState()
    {
        return state;
    }

    public String getTypeImmobile()
    {
        return typeImmobile;
    }

    public String getSize()
    {
        return size;
    }

    public String getBedroom()
    {
        return bedroom;
    }

    public String getSuite()
    {
        return suite;
    }

    public String getBathroom()
    {
        return bathroom;
    }

    public String getVacancy()
    {
        return vacancy;
    }

    public String getTradingType()
    {
        return tradingType;
    }

    public String getPrice()
    {
        return price;
    }

    public String getCondominium()
    {
        return condominium;
    }

    public String getIptu()
    {
        return iptu;
    }

    public String getTitle()
    {
        return title;
    }

    public String getDescription()
    {
        return description;
    }

    public Calendar getDate()
    {
        Calendar calendar = new GregorianCalendar();
        calendar.getTime();
        return calendar;
    }

    public String getUserId()
    {
        return userId;
    }

    public void addImage(Image image)
    {
        this.images.add(image);
    }

    public ArrayList<Image> getImages()
    {
        return images;
    }
}
