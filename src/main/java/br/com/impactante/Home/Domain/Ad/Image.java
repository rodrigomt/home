package br.com.impactante.Home.Domain.Ad;

public class Image
{
    private String id;
    private String url;
    private String id_ad;

    public Image(
        String id,
        String url,
        String id_ad
    ) {
        this.id     = id;
        this.url    = url;
        this.id_ad  = id_ad;
    }

    public String getIdImage()
    {
        return id;
    }

    public String getUrl()
    {
        return url;
    }

    public String getId_ad()
    {
        return id_ad;
    }
}