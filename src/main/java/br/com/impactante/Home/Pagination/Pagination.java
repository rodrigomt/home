package br.com.impactante.Home.Pagination;

import br.com.impactante.Home.Application.AdApplication;
import br.com.impactante.Home.Interface.Ad.View.Model.Ad;

import java.util.List;

public class Pagination
{
    private List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> currentPageAds;
    private List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> allAds;
    private int pageCurrent;
    private int numberPages;

    public Pagination(List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> allAdsTrading, List<br.com.impactante.Home.Interface.Ad.View.Model.Ad> currentPageAds, int pageCurrent)
    {
        this.pageCurrent    = pageCurrent;
        this.allAds         = allAdsTrading;
        this.currentPageAds = currentPageAds;

        computerNumberPages();
    }

    public List<Ad> getIterator()
    {
        return currentPageAds;
    }

    public boolean hasPrevious()
    {
        return (pageCurrent > 1);
    }

    private void computerNumberPages()
    {
        this.numberPages = (int) Math.ceil( (double) allAds.size() / (double) 5 );
    }

    public int getNumberPages()
    {
        return numberPages;
    }

    public int getPreviousNumberPage()
    {
        return pageCurrent - 1;
    }

    public int getNextNumberPage()
    {
        return  pageCurrent + 1;
    }

    public boolean isPageValid()
    {
        return  pageCurrent <= numberPages;
    }

    public boolean hasNext()
    {
        return pageCurrent < numberPages;
    }

    public boolean lastPage()
    {
        return pageCurrent == numberPages;
    }

}
