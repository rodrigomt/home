<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="br.com.impactante.Home.Infrastructure.Authentication.Session" %>
<%Session sessionLogin = new Session();%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no"/>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/util.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/button.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/color.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/form.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/grid.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/modal.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/nav.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/background.css"/>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/form-ad.css"/>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/img/home-logo.png"/>
        <title>Home</title>
    </head>
    <body>
        <%if (sessionLogin.IsValid(request.getCookies())) {%>
        <div class="navbar">
            <div class="container-fluid">
                <div class="navbar-nav">
                    <div class="navbar-nav-element col-4 d-none-sm">
                        <a href="/myAds">Meus anúncios</a>
                    </div>
                    <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                        <a href="/"><img src="<%=request.getContextPath()%>/img/home-logo.png" width="50"></a>
                    </div>
                    <div class="navbar-nav-element col-4 text-right d-none-sm position-relative">
                        <a class="btn btn-orange" href="/newAd">Anunciar imóvel</a>
                        <a class="profile btn btn-profile"><%=sessionLogin.getCurrentUserName().substring(0, 2)%></a>
                        <div class="sub-menu position-absolute d-none">
                            <ul>
                                <li><p><%=sessionLogin.getCurrentUserName()%></p></li>
                                <li><a href="/myAccount">Minha Conta</a></li>
                                <li><a href="/configs">Configurações</a></li>
                                <form action="/logout" method="post">
                                    <li><input class="btn-logout" type="submit" value="Sair"></li>
                                </form>
                            </ul>
                        </div>
                    </div>
                    <button class="navbar-collapse-btn js-btn-collapse">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt text-center">
                    <ul>
                        <form action="/logout" method="post">
                            <li><a class="btn btn-primary d-block btn-block" href="/myAccount">Minha Conta</a></li>
                            <li><a class="btn btn-orange d-block btn-block mt-2" href="/newAd">Anunciar imóvel</a></li>
                            <li><a class="btn btn-primary d-block btn-block mt-2" href="/myAds">Meus Anúncios</a></li>
                            <li><input class="btn btn-logout d-block btn-block mt-2" type="submit" value="Sair"></li>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
        <% } else { %>
        <div class="navbar">
            <div class="container-fluid">
                <div class="navbar-nav">
                    <div class="navbar-nav-element col-4 d-none-sm">
                        <a href="/announce">Anunciar imóvel</a>
                    </div>
                    <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                        <a href="/"><img src="<%=request.getContextPath()%>/img/home-logo.png" width="50"></a>
                    </div>
                    <div class="navbar-nav-element col-4 text-right d-none-sm">
                        <a class="btn btn-primary" href="/login">Entrar</a>
                    </div>
                    <button class="navbar-collapse-btn js-btn-collapse">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                    <a class="btn btn-primary btn-block d-block" href="/login">Entrar</a>
                </div>
            </div>
        </div>
        <%}%>
        <div class="container-fluid container-fluid-m h-vh background-img d-flex items-align-center justify-content-between">
            <div class="container">
                <div class="w-100 p-3 d-flex flex-column mt-3">
                    <h1 class="mt-default text-size text-shadow text-center text-white">Encontre aqui seu novo lar!</h1>
                    <div class="card-search w-60 bg-white m-auto mt-3 text-center box-sh b-rds">
                        <h3 class="w-100 text-muted">O que você deseja ?</h3>
                        <form action="/adsList" class="mt-default mb-3 p-3">
                            <div class="position-relative">
                                <div class="btn bg-primary radio-select w-40 js-rent-select mb-20 mt-default">Alugar</div>
                                <div class="btn bg-muted radio-select w-40 js-sale-select mb-20 mt-default">Comprar</div>
                                <input name="order" type="hidden" value="0"/>
                                <input name="page" type="hidden" value="1"/>
                                <input name="search" class="w-60 js-field" type="text" autocomplete="off"/>
                                <input name="option" class="js-trading-option" type="hidden" value="1"/>
                                <button class="btn btn-orange btn-search w-20 d-inline-block">Pesquisar</button>
                                <div class="result-search position-absolute"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath()%>/js/mobile.js"></script>
        <script src="<%=request.getContextPath()%>/js/ajax.search.js"></script>
        <script src="<%=request.getContextPath()%>/js/search.js"></script>
        <%if (sessionLogin.IsValid(request.getCookies())) {%>
            <script src="/js/menu.perfil.user.js"></script>
            <script src="/js/sign-out.js"></script>
        <%}%>
    </body>
</html>