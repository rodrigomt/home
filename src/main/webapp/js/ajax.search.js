;(function () {

    let field              = document.querySelector('.js-field'),
        rentOption         = document.querySelector('.js-rent-select'),
        saleOption         = document.querySelector('.js-sale-select'),
        inputTradingOption = document.querySelector('.js-trading-option');

    let search;

    getParameters();

    rentOption.addEventListener('click', function () {
        rentOption.classList.add('bg-primary');
        rentOption.classList.remove('bg-muted');
        saleOption.classList.remove('bg-primary');
        saleOption.classList.add('bg-muted');
        inputTradingOption.removeAttribute("value");
        inputTradingOption.setAttribute("value", "1");
        search = 1;
    });

    saleOption.addEventListener('click', function () {
        saleOption.classList.add('bg-primary');
        saleOption.classList.remove('bg-muted');
        rentOption.classList.remove('bg-primary');
        rentOption.classList.add('bg-muted');
        inputTradingOption.removeAttribute("value");
        inputTradingOption.setAttribute("value", "0");
        search = 2;
    });

    field.addEventListener('input', function () {
        let list = document.querySelectorAll('.div-internal-result');

        if(this.value.length > 0) {
            for (let i = 0; i < list.length ; i++) {
                let ad = list[i];
                let title = ad.querySelector('h3');
                let exp = new RegExp(this.value, "i");
                console.log(title.textContent);
                console.log(exp.test(title.textContent));
                if(!exp.test(title.textContent)) {
                    ad.classList.add('d-none');
                } else {
                    ad.classList.remove('d-none');
                }
            }
        } else {
            for (let i = 0; i < list.length; i++) {
                let ad = list[i];
                ad.classList.add('d-none');
            }
        }

    });

    function getParameters() {

        let query            = location.search.slice(1),
            parameters       = query.split('&');

        parameters.forEach(function (parameter, index, array) {
            let parameterNameValue = parameter.split('='),
                name               = parameterNameValue[0],
                value              = parameterNameValue[1];

            addValue(index, value);
        });
    }

    function addValue(index, value) {
        if(value === '1') {
            rentOption.classList.add('bg-primary');
            rentOption.classList.remove('bg-muted');
            saleOption.classList.remove('bg-primary');
            saleOption.classList.add('bg-muted');
            search = 1;
        }

        if(value === '0') {
            saleOption.classList.add('bg-primary');
            saleOption.classList.remove('bg-muted');
            rentOption.classList.remove('bg-primary');
            rentOption.classList.add('bg-muted');
            search = 2;
        }
    }

} ());