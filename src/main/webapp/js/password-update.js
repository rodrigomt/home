;(function () {

    let div                = document.getElementById('preload'),
        msg                = document.querySelector('.msg'),
        overlay            = document.querySelector('.overlay'),
        newPassword        = document.querySelector('.new-password'),
        confirmNewPassword = document.querySelector('.confirm-new-password'),
        btnSendNewPassword = document.querySelector('.btn-send-new-password');

    btnSendNewPassword.addEventListener('click', function () {

        let boxAlert = document.querySelector('.box-alert');

        boxAlert.classList.remove('div-alert-two');
        boxAlert.setAttribute('hidden', 'hidden');

        msg.textContent = '';
        msg.setAttribute('hidden', 'hidden');

        newPassword.classList.remove('input-alert');
        confirmNewPassword.classList.remove('input-alert');

        if(newPassword.value !== '' && confirmNewPassword.value !== '') {

            overlay.removeAttribute('hidden');
            overlay.classList.add('overlay-complements');

            div.classList.add('spinner');

            if (newPassword.value === confirmNewPassword.value) {

                let code = document.querySelector('.form-password').getAttribute('data-id');

                let xhr = new XMLHttpRequest();

                xhr.open('post', '/password');
                xhr.addEventListener('load', function () {

                    if (xhr.responseText === 'updated') {

                        let success      = document.querySelector('.success'),
                            textInfo     = document.querySelector('.text-info'),
                            formPassword = document.querySelector('.form-password');

                        success.removeAttribute('hidden');
                        formPassword.setAttribute('hidden', 'hidden');
                        textInfo.textContent = 'Processo finalizado'

                        msg.textContent = '';
                        msg.setAttribute('hidden', 'hidden');

                        overlay.setAttribute('hidden', 'hidden');
                        overlay.classList.remove('overlay-complements');

                        div.classList.remove('spinner');

                    } else {

                        msg.removeAttribute('hidden');
                        msg.textContent = 'Não foi possivel atualizar a senha';

                        overlay.setAttribute('hidden', 'hidden');
                        overlay.classList.remove('overlay-complements');

                        div.classList.remove('spinner');

                    }
                });

                xhr.send(`id=${code}&newPassword=${newPassword.value}`);

            } else {

                overlay.setAttribute('hidden', 'hidden');
                overlay.classList.remove('overlay-complements');

                div.classList.remove('spinner');

                boxAlert.classList.add('div-alert-two');
                boxAlert.removeAttribute('hidden');

                newPassword.classList.add('input-alert');
                confirmNewPassword.classList.add('input-alert');

                msg.removeAttribute('hidden');
                msg.textContent = 'As senhas não correspondem';
            }
        }
        if(newPassword.value === '' && confirmNewPassword.value !== ''){

            overlay.setAttribute('hidden', 'hidden');
            overlay.classList.remove('overlay-complements');

            div.classList.remove('spinner');

            boxAlert.classList.add('div-alert-two');
            boxAlert.removeAttribute('hidden');

            newPassword.classList.add('input-alert');
            confirmNewPassword.classList.remove('input-alert');

            msg.removeAttribute('hidden');
            msg.textContent = 'Crie uma nova senha';

        }

        if(newPassword.value !== '' && confirmNewPassword.value === ''){

            overlay.setAttribute('hidden', 'hidden');
            overlay.classList.remove('overlay-complements');

            div.classList.remove('spinner');

            boxAlert.classList.add('div-alert-two');
            boxAlert.removeAttribute('hidden');

            newPassword.classList.remove('input-alert');
            confirmNewPassword.classList.add('input-alert');

            msg.removeAttribute('hidden');
            msg.textContent = 'Confirme a senha criada';

        }

        if(newPassword.value === '' && confirmNewPassword.value === ''){

            overlay.setAttribute('hidden', 'hidden');
            overlay.classList.remove('overlay-complements');

            div.classList.remove('spinner');

            boxAlert.classList.add('div-alert-two');
            boxAlert.removeAttribute('hidden');

            newPassword.classList.add('input-alert');
            confirmNewPassword.classList.add('input-alert');

            msg.removeAttribute('hidden');
            msg.textContent = 'Os campos não podem está em branco';

        }
    });

} ());