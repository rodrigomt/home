;(function () {

    let overlay       = document.querySelector('.overlay'),
        div           = document.getElementById('preload'),
        buttonCreate  = document.querySelector('.enter');

    buttonCreate.addEventListener('click', function () {
        overlay.removeAttribute('hidden');
        overlay.classList.add('overlay-complements');
        div.classList.add('spinner');
    });

} ());