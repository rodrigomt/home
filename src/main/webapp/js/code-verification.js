;(function () {
    let codeVerification = document.querySelector('.code'),
        btnSendCode      = document.querySelector('.btn-send-code'),
        msg              = document.querySelector('.msg'),
        overlay          = document.querySelector('.overlay'),
        div              = document.getElementById('preload');

    btnSendCode.addEventListener('click', function () {

        let boxAlert = document.querySelector('.box-alert');

        boxAlert.classList.remove('div-alert-two');
        boxAlert.setAttribute('hidden', 'hidden');

        msg.textContent = '';
        msg.setAttribute('hidden', 'hidden');

        codeVerification.classList.remove('input-alert');

        if(codeVerification.value !== "") {

            let code = codeVerification.value;

            overlay.removeAttribute('hidden');
            overlay.classList.add('overlay-complements');

            div.classList.add('spinner');

            let xhr = new XMLHttpRequest();
            xhr.open('post', '/codeVerification');
            xhr.addEventListener('load', function () {
                if (xhr.responseText === 'valid') {

                    let formCode = document.querySelector('.form-code'),
                        formPassword = document.querySelector('.form-password'),
                        textInfo = document.querySelector('.text-info');

                    formCode.setAttribute('hidden', 'hidden');
                    formPassword.setAttribute('data-id', code);
                    formPassword.removeAttribute('hidden');
                    textInfo.textContent = 'Crie sua nova senha';

                    let boxAlert = document.querySelector('.box-alert');

                    boxAlert.setAttribute('hidden', 'hidden');
                    boxAlert.classList.remove('div-alert');

                    msg.textContent = '';
                    msg.setAttribute('hidden', 'hidden');

                    codeVerification.classList.remove('input-alert');

                    overlay.setAttribute('hidden', 'hidden');
                    overlay.classList.remove('overlay-complements');

                    div.classList.remove('spinner');

                } else {

                    let boxAlert = document.querySelector('.box-alert');

                    boxAlert.classList.add('div-alert-two');
                    boxAlert.removeAttribute('hidden');

                    msg.removeAttribute('hidden');
                    msg.textContent = 'o oódigo não é válido';

                    codeVerification.classList.add('input-alert');

                    overlay.setAttribute('hidden', 'hidden');
                    overlay.classList.remove('overlay-complements');

                    div.classList.remove('spinner');

                }
            });
            xhr.send(`code=${codeVerification.value}`);
        } else {

            let boxAlert = document.querySelector('.box-alert');

            boxAlert.classList.add('div-alert-two');
            boxAlert.removeAttribute('hidden');

            msg.removeAttribute('hidden');
            msg.textContent = 'O campo não pode está vazio';

            codeVerification.classList.add('input-alert');

            overlay.setAttribute('hidden', 'hidden');
            overlay.classList.remove('overlay-complements');

            div.classList.remove('spinner');
        }
    });
} ());