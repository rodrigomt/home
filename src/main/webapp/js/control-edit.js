;(function () {
    let arrow = document.querySelectorAll(".js-arrow");

    arrow.forEach(function (el,i,array)
    {
        el.addEventListener("click", function ()
        {
            let arrow = document.querySelectorAll(".js-arrow");
            let card  = document.querySelectorAll(".js-card");

            if(arrow[i].getAttribute("class") == "d-inline-block arrow-down js-arrow"
                || arrow[i].getAttribute("class") == "d-inline-block js-arrow arrow-down")
            {
                arrow[i].classList.remove("arrow-down");
                card[i].classList.remove("display");
                arrow[i].classList.add("arrow-up");
                card[i].classList.add("card-body");
                if(i == 0){
                    card[i].classList.add("text-center");
                }
                return;
            }

            if(arrow[i].getAttribute("class") == "d-inline-block js-arrow arrow-up")
            {
                arrow[i].classList.remove("arrow-up");
                card[i].classList.remove("card-body");
                arrow[i].classList.add("arrow-down");
                card[i].classList.add("display");
                if(i == 0){
                    card[i].classList.remove("text-center");
                }
                return;
            }

        });
    });
} ());