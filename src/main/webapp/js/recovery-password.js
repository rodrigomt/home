;(function() {

    let email       = document.querySelector('.email'),
        btnSend     = document.querySelector('.btn-send'),
        textInfo    = document.querySelector('.text-info'),
        formRecover = document.querySelector('.form-recover'),
        formCode    = document.querySelector('.form-code'),
        overlay     = document.querySelector('.overlay'),
        div         = document.getElementById('preload');

    btnSend.addEventListener('click', function () {

        let boxAlert = document.querySelector('.box-alert'),
            msg      = document.querySelector('.msg');

        boxAlert.classList.remove('div-alert-two');
        boxAlert.setAttribute('hidden', 'hidden');

        msg.textContent = '';
        msg.setAttribute('hidden', 'hidden');

        email.classList.remove('input-alert');

        if(email.value !== "") {

            overlay.removeAttribute('hidden');
            overlay.classList.add('overlay-complements');
            div.classList.add('spinner');

            let xhr = new XMLHttpRequest();

            xhr.open('post', '/recoverPassword');
            xhr.addEventListener('load', function () {

                if(xhr.responseText === 'enviado') {

                    let msg = document.querySelector('.msg'),
                        boxAlert = document.querySelector('.box-alert');

                    boxAlert.setAttribute('hidden', 'hidden');
                    boxAlert.classList.remove('div-alert');

                    msg.textContent = '';
                    msg.setAttribute('hidden', 'hidden');

                    email.classList.remove('input-alert-two');

                    textInfo.textContent = 'Digite o código recebido';

                    formRecover.setAttribute('hidden', 'hidden');
                    formCode.removeAttribute('hidden');

                    overlay.setAttribute('hidden', 'hidden');
                    overlay.classList.remove('overlay-complements');

                    div.classList.remove('spinner');

                } else {

                    let msg = document.querySelector('.msg'),
                        boxAlert = document.querySelector('.box-alert');

                    boxAlert.classList.add('div-alert-two');
                    boxAlert.removeAttribute('hidden');

                    msg.removeAttribute('hidden');
                    msg.textContent = 'Email Informado Não é Cadastrado';

                    email.classList.add('input-alert');

                    overlay.setAttribute('hidden', 'hidden');
                    overlay.classList.remove('overlay-complements');

                    div.classList.remove('spinner');
                }
            });

            xhr.send(`email=${email.value}`);

        } else {

            let msg = document.querySelector('.msg'),
                boxAlert = document.querySelector('.box-alert');

            boxAlert.classList.add('div-alert-two');
            boxAlert.removeAttribute('hidden');

            msg.removeAttribute('hidden');
            msg.textContent = 'O campo não pode está vazio';

            email.classList.add('input-alert');

            overlay.setAttribute('hidden', 'hidden');
            overlay.classList.remove('overlay-complements');

            div.classList.remove('spinner');
        }
    });
} ());