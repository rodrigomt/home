(function () {

    let dialog = document.querySelector('dialog'),
        overlay = document.querySelector('.overlay');

    dialog.classList.add('dialog');

    document.querySelector('.edit-data').onclick = function() {

        let data = this.getAttribute('data'),
            name = document.querySelector('#name-user'),
            email = document.querySelector('#email-user'),
            fone  = document.querySelector('#fone-user');

        elementGenerator(data, name, email, fone);

        dialog.show();
        overlay.removeAttribute('hidden');
    }

    document.querySelector('.update-password').onclick = function() {

        let data = this.getAttribute('data');
        elementGenerator(data);
        dialog.show();
        overlay.removeAttribute('hidden');
    }

    document.querySelector('.delete-account').onclick = function() {

        let data = this.getAttribute('data');
        elementGenerator(data);
        dialog.show();
        overlay.removeAttribute('hidden');
    }

    function elementGenerator(data, name, email, fone) {

        let divCard         = document.createElement('div'),
            h1              = document.createElement('h1'),
            divAlert        = document.createElement('div'),
            div             = document.createElement('div'),
            divOverlay      = document.createElement('div'),
            divSpinner      = document.createElement('div'),
            form            = document.createElement('form'),
            labelPrimary    = document.createElement('label'),
            labelSecondary  = document.createElement('label'),
            labelTertiary   = document.createElement('label'),
            inputPrimary    = document.createElement('input'),
            inputSecondary  = document.createElement('input'),
            inputTertiary   = document.createElement('input'),
            span            = document.createElement('span');

        formAssembler(divCard, h1, divAlert, div, divOverlay, divSpinner, form, labelPrimary,
                      labelSecondary, labelTertiary, inputPrimary, inputSecondary,
                      inputTertiary, data, name, email, fone, span);
    }

    function buttonsGenerator(form, data, divAlert, divOverlay, divSpinner) {

        let divButtons    = document.createElement('div'),
            buttonConfirm = document.createElement('button'),
            buttonCancel  = document.createElement('button');

        buttonCancel.setAttribute('type', 'button');
        buttonCancel.addEventListener('click', function (qualifiedName, value){
            overlay.setAttribute('hidden', "");
            dialog.close();
            dialog.textContent = '';
        });

        buttonConfirm.setAttribute('type', 'button');

        buttonConfirm.textContent = 'Confirmar';
        buttonCancel.textContent  = 'Cancelar';

        buttonConfirm.classList.add('btn');
        buttonConfirm.classList.add('btn-primary');

        buttonCancel.classList.add('btn');
        buttonCancel.classList.add('btn-secondary');

        buttonCancel.classList.add('m-5');
        buttonConfirm.classList.add('m-5');

        if(data === 'delete') {
            divButtons.classList.add('text-center');
        } else {
            divButtons.classList.add('text-right');
        }

        divButtons.appendChild(buttonCancel);
        divButtons.appendChild(buttonConfirm);
        form.appendChild(divButtons);


            buttonConfirm.addEventListener('click', function (qualifiedName, value){

                if(data === 'edit-data') {

                    let name = document.getElementById('form-name').getAttribute('value'),
                        email = document.getElementById('form-email').getAttribute('value'),
                        phone = document.getElementById('form-phone').getAttribute('value'),
                        inputName = document.querySelector('.input-name'),
                        inputEmail = document.querySelector('.input-email'),
                        inputPhone = document.querySelector('.input-phone'),
                        alert = document.querySelector('.alert');

                    inputName.classList.remove('input-alert');
                    inputEmail.classList.remove('input-alert');
                    inputPhone.classList.remove('input-alert');
                    alert.textContent = '';

                    validateFormOneInput(name, email, phone ,inputName, inputEmail, inputPhone, alert, data, divAlert);
                    validateFormTwoInput(name, email, phone ,inputName, inputEmail, inputPhone, alert, data, divAlert);
                    validateFormAllInput(name, email, phone ,inputName, inputEmail, inputPhone, alert, data, divAlert);

                    if (name.length >= 1 && email.length >= 1 && phone.length >= 1) {

                        divOverlay.removeAttribute('hidden');
                        divOverlay.classList.add('position-absolute');
                        divSpinner.classList.add('spinner');
                        divAlert.classList.remove('div-alert');

                        let xhr     = new XMLHttpRequest(),
                            form    = new FormData();



                        xhr.open('POST', '/editAccount');
                        xhr.addEventListener('load', function() {

                            if(xhr.status === 200) {
                                let result = xhr.responseText;
                                if(result === 'registrado') {
                                    divOverlay.setAttribute('hidden', '');
                                    divOverlay.classList.remove('position-absolute');
                                    divSpinner.classList.remove('spinner');
                                    divAlert.classList.add('div-alert');
                                    alert.textContent = 'Email informado já registrado';
                                    inputEmail.classList.add('input-alert');
                                } else {
                                    document.location = document.location;
                                }
                            }
                        });

                        form.append('name', name);
                        form.append('email', email);
                        xhr.send(form);
                    }
                }

                if(data === 'edit-password') {

                    let password = document.getElementById('form-password').getAttribute('value'),
                        newPassword = document.getElementById('form-new-password').getAttribute('value'),
                        passwordConfirm = document.getElementById('form-confirm-new-password').getAttribute('value'),
                        inputPassword = document.querySelector('.input-password'),
                        inputNewPassword = document.querySelector('.input-new-password'),
                        inputConfirmNewPassword = document.querySelector('.input-confirm-new-password'),
                        alert = document.querySelector('.alert');

                    inputPassword.classList.remove('input-alert');
                    inputNewPassword.classList.remove('input-alert');
                    inputConfirmNewPassword.classList.remove('input-alert');
                    alert.textContent = '';

                    validateFormOneInput(password, newPassword, passwordConfirm, inputPassword, inputNewPassword, inputConfirmNewPassword, alert, data, divAlert);
                    validateFormTwoInput(password, newPassword, passwordConfirm, inputPassword, inputNewPassword, inputConfirmNewPassword, alert, data, divAlert);
                    validateFormAllInput(password, newPassword, passwordConfirm, inputPassword, inputNewPassword, inputConfirmNewPassword, alert, divAlert);

                    if (password.length >= 1 && newPassword.length >= 1 && passwordConfirm.length >= 1) {

                            if(newPassword === passwordConfirm) {

                                divOverlay.removeAttribute('hidden');
                                divOverlay.classList.add('position-absolute');
                                divSpinner.classList.add('spinner');
                                divAlert.classList.remove('div-alert');
                                alert.textContent = '';

                                let xhr = new XMLHttpRequest(),
                                    form = new FormData();

                                xhr.open('POST', '/updatePassword');
                                xhr.addEventListener('load', function () {

                                    if (xhr.status === 200) {
                                        if (xhr.responseText === 'Autentificado') {
                                            document.location = document.location;
                                        } else {
                                            divOverlay.setAttribute('hidden', '');
                                            divOverlay.classList.remove('position-absolute');
                                            divSpinner.classList.remove('spinner');
                                            divAlert.classList.add('div-alert');
                                            inputPassword.classList.add('input-alert');
                                            alert.textContent = 'Senha atual errada.';
                                        }
                                    }
                                });

                                form.append('password', password);
                                form.append('new-password', newPassword);
                                form.append('new-password-confirm', passwordConfirm);
                                xhr.send(form);
                            } else {
                                alert.textContent = 'As senhas não conrespondem.';
                                inputNewPassword.classList.add('input-alert');
                                inputConfirmNewPassword.classList.add('input-alert');

                            }
                        }
                }

                if(data === 'delete') {

                    let password = document.getElementById('form-password').getAttribute('value'),
                        inputPassword = document.querySelector('.input-password'),
                        alert = document.querySelector('.alert');

                    validateFormOneInput(password, '', '' ,inputPassword, '', '', alert, data, divAlert);

                    if (password.length >= 1) {

                        divOverlay.removeAttribute('hidden');
                        divOverlay.classList.add('position-absolute');
                        divSpinner.classList.add('spinner');
                        divAlert.classList.remove('div-alert');

                        let xhr = new XMLHttpRequest(),
                            formData = new FormData();

                        xhr.open('POST', '/deleteAccount');
                        xhr.addEventListener('load', function () {

                            if (xhr.status === 200) {
                                if(xhr.responseText === '/') {
                                    document.location = xhr.responseText;
                                } else {
                                    divOverlay.setAttribute('hidden', '');
                                    divOverlay.classList.remove('position-absolute');
                                    divSpinner.classList.remove('spinner');
                                    divAlert.classList.add('div-alert');
                                    inputPassword.classList.add('input-alert');
                                    alert.textContent = 'Senha informada está errada.';
                                }
                            }
                        });

                        formData.append('password', password)
                        xhr.send(formData);
                    }
                }

            });
    }

    function formAssembler(divCard, h1, divAlert, div, divOverlay, divSpinner, form, labelPrimary,
                           labelSecondary, labelTertiary, inputPrimary, inputSecondary,
                           inputTertiary, data, name, email, fone, span) {

        if(data === 'edit-data') {

            h1.textContent              = 'Editando dados da conta';
            h1.classList.add('m-default');
            labelPrimary.textContent    = 'Nome';
            labelSecondary.textContent  = 'Email';
            labelTertiary.textContent   = 'Telefone';

            labelPrimary.setAttribute('for', 'form-name');
            labelSecondary.setAttribute('for', 'form-email');
            labelTertiary.setAttribute('for', 'form-phone');


            inputPrimary.setAttribute('value', name.textContent);
            inputSecondary.setAttribute('value', email.textContent);
            inputTertiary.setAttribute('value', fone.textContent);

            inputPrimary.setAttribute('type', 'text');
            inputSecondary.setAttribute('type', 'email');
            inputTertiary.setAttribute('type', 'text');


            inputPrimary.classList.add('input-name');
            inputSecondary.classList.add('input-email');
            inputTertiary.classList.add('input-phone');

            inputPrimary.addEventListener('input', function() {
               inputPrimary.setAttribute('value', this.value);
            });

            inputSecondary.addEventListener('input', function () {
                inputSecondary.setAttribute('value', this.value);
            });

            inputTertiary.addEventListener('input', function() {
               inputTertiary.setAttribute('value', this.value);
            });

            inputPrimary.setAttribute('id', 'form-name');
            inputSecondary.setAttribute('id', 'form-email');
            inputTertiary.setAttribute('id', 'form-phone');

            divCard.classList.add('card-header');
            divCard.classList.add('text-center');
            div.classList.add('form');
            form.classList.add('form-data');
            form.classList.add('padding-20');
            span.classList.add('alert');

            divOverlay.classList.add('overlay-secondary');
            dialog.classList.add('position-relative');
            divOverlay.setAttribute('hidden', '');


            divOverlay.appendChild(divSpinner);
            form.appendChild(labelPrimary);
            form.appendChild(inputPrimary);
            form.appendChild(labelSecondary);
            form.appendChild(inputSecondary);
            form.appendChild(labelTertiary);
            form.appendChild(inputTertiary);
            buttonsGenerator(form, data, divAlert, divOverlay, divSpinner);
            divCard.appendChild(h1);
            divAlert.appendChild(span);
            div.appendChild(form);
            divOverlay.appendChild(divSpinner);
            dialog.appendChild(divOverlay);
            dialog.appendChild(divCard);
            dialog.appendChild(divAlert)
            dialog.appendChild(div);

        }

        if(data === 'edit-password') {

            h1.textContent              = 'Redefinindo senha';
            h1.classList.add('m-default');
            labelPrimary.textContent    = 'Senha atual';
            labelSecondary.textContent  = 'Nova senha';
            labelTertiary.textContent   = 'Confirme nova senha';

            labelPrimary.setAttribute('for', 'form-password');
            labelSecondary.setAttribute('for', 'form-new-password');
            labelTertiary.setAttribute('for', 'form-confirm-new-password');

            inputPrimary.setAttribute('id', 'form-password');
            inputSecondary.setAttribute('id', 'form-new-password');
            inputTertiary.setAttribute('id', 'form-confirm-new-password');

            inputPrimary.setAttribute('type', 'password');
            inputSecondary.setAttribute('type', 'password');
            inputTertiary.setAttribute('type', 'password');

            inputPrimary.setAttribute('value', '');
            inputSecondary.setAttribute('value', '');
            inputTertiary.setAttribute('value', '');

            inputPrimary.classList.add('input-password');
            inputSecondary.classList.add('input-new-password');
            inputTertiary.classList.add('input-confirm-new-password');


            inputPrimary.addEventListener('input', function() {
                inputPrimary.setAttribute('value', this.value);
            });

            inputSecondary.addEventListener('input', function () {
                inputSecondary.setAttribute('value', this.value);
            });

            inputTertiary.addEventListener('input', function() {
                inputTertiary.setAttribute('value', this.value);
            });

            span.classList.add('alert');
            divCard.classList.add('card-header');
            div.classList.add('form');

            divCard.classList.add('text-center');
            form.classList.add('padding-20');

            divOverlay.classList.add('overlay-secondary');
            divOverlay.setAttribute('hidden', '');

            divOverlay.appendChild(divSpinner);
            form.appendChild(labelPrimary);
            form.appendChild(inputPrimary);
            form.appendChild(labelSecondary);
            form.appendChild(inputSecondary);
            form.appendChild(labelTertiary);
            form.appendChild(inputTertiary);
            buttonsGenerator(form, data, divAlert, divOverlay, divSpinner);
            divCard.appendChild(h1);
            divAlert.appendChild(span);
            div.appendChild(form);
            dialog.appendChild(divCard);
            dialog.appendChild(divAlert);
            dialog.appendChild(divOverlay);
            dialog.appendChild(div);
        }

        if(data === 'delete') {
            h1.textContent              = 'Deseja excluir sua conta?';
            labelPrimary.textContent = 'Informe sua senha'
            h1.classList.add('m-default');

            div.classList.add('form');
            divCard.classList.add('card-header');
            divAlert.classList.add('div-alert-name');
            divCard.classList.add('text-center');
            form.classList.add('padding-20');

            span.classList.add('alert');

            labelPrimary.setAttribute('for', 'form-password');
            inputPrimary.setAttribute('id', 'form-password');
            inputPrimary.setAttribute('type', 'password');
            inputPrimary.setAttribute('value', '');
            inputPrimary.classList.add('input-password');

            inputPrimary.addEventListener('input', function() {
                inputPrimary.setAttribute('value', this.value);
            });

            divOverlay.classList.add('overlay-secondary');
            divOverlay.setAttribute('hidden', '');

            divOverlay.appendChild(divSpinner);
            form.appendChild(labelPrimary);
            form.appendChild(inputPrimary);
            buttonsGenerator(form, data, divAlert, divOverlay, divSpinner);
            div.appendChild(form);
            divCard.appendChild(h1);
            divAlert.appendChild(span);
            dialog.appendChild(divCard);
            dialog.appendChild(divAlert);
            dialog.appendChild(divOverlay);
            dialog.appendChild(div);
        }

    }


    function validateFormAllInput(primaryValue, secondaryValue, tertiaryValue, primaryInput, secondaryInput, tertiaryInput, alert, divAlert) {
        if(primaryValue === '' && secondaryValue === '' && tertiaryValue === '') {

            primaryInput.classList.add('input-alert');
            secondaryInput.classList.add('input-alert');
            tertiaryInput.classList.add('input-alert');
            alert.textContent = 'Os campos não podem está em branco.';

        }
    }

    function validateFormOneInput(primaryValue, secondaryValue, tertiaryValue, primaryInput, secondaryInput, tertiaryInput, alert, data, divAlert) {

        if (data === 'delete')
        {
            if (primaryValue === '') {
                divAlert.classList.add('div-alert');
                primaryInput.classList.add('input-alert');
                alert.textContent = 'Informe sua senha.';
            }

        } else {

            if (primaryValue === '') {
                primaryInput.classList.add('input-alert');
                if (data === 'edit-data') {
                    divAlert.classList.add('div-alert');
                    alert.textContent = 'Informe seu nome.';
                } else {
                    divAlert.classList.add('div-alert');
                    alert.textContent = 'Informe a senha atual.';
                }
            }

            if (secondaryValue === '') {
                secondaryInput.classList.add('input-alert');
                if (data === 'edit-data') {
                    divAlert.classList.add('div-alert');
                    alert.textContent = 'Informe seu email.';
                } else {
                    divAlert.classList.add('div-alert');
                    alert.textContent = 'Crie a nova senha.';
                }
            }

            if (tertiaryValue === '') {
                tertiaryInput.classList.add('input-alert');
                if (data === 'edit-data') {
                    divAlert.classList.add('div-alert');
                    alert.textContent = 'Informe seu telefone.';
                } else {
                    divAlert.classList.add('div-alert');
                    alert.textContent = 'Confirme sua nova senha.';
                }
            }
        }
    }

    function validateFormTwoInput(primaryValue, secondaryValue, tertiaryValue, primaryInput, secondaryInput, tertiaryInput, alert, data, divAlert) {
        if(primaryValue === '' &&  secondaryValue === '') {
            primaryInput.classList.add('input-alert');
            secondaryInput.classList.add('input-alert');
            if(data === 'edit-data') {
                divAlert.classList.add('div-alert');
                alert.textContent = 'Nome e email estão em branco.';
            } else {
                divAlert.classList.add('div-alert');
                alert.textContent = 'Senha atual e nova senha estão em branco.';
            }
        }

        if(primaryValue === '' && tertiaryValue === '') {
            primaryInput.classList.add('input-alert');
            tertiaryInput.classList.add('input-alert');
            if(data === 'edit-data') {
                divAlert.classList.add('div-alert');
                alert.textContent = 'Nome e telefone estão em branco.';
            } else {
                divAlert.classList.add('div-alert');
                alert.textContent = 'Senha atual e a confirmação estão em branco.';
            }
        }

        if(secondaryValue === '' && tertiaryValue === '' ) {
            secondaryInput.classList.add('input-alert');
            tertiaryInput.classList.add('input-alert');
            if(data === 'edit-data') {
                divAlert.classList.add('div-alert');
                alert.textContent = 'email e telefone estão em branco.';
            } else {
                divAlert.classList.add('div-alert');
                alert.textContent = 'crie a nova senha e confirme.';
            }
        }
    }

} ());