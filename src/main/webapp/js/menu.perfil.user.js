let buttonProfile = document.querySelector('.profile');

buttonProfile.addEventListener('click', function() {
    let div = document.querySelector('.sub-menu');
    if(div.getAttribute('class') === 'sub-menu position-absolute d-none') {
        div.classList.remove('d-none');
    } else {
        div.classList.add('d-none');
    }
});