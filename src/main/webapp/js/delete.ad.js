let buttonDelete = document.querySelectorAll('.js-delete');
let url = document.querySelectorAll('.image-url');
buttonDelete.forEach(function (el, index, array) {
    el.addEventListener('click', function () {
        let id = el.getAttribute('id');

        let formData = new FormData();
        formData.append('id', id);
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/deleteAd');
        xhr.addEventListener('load', function () {
            if(xhr.status === 200)
            {
                window.location.reload();
            }
        });
        xhr.send(formData);
        deleteImages();

    });
});

function deleteImages()
{
    let formData = new FormData();
    url.forEach(function (el, index, array) {
        formData.append("url" + index, el.textContent);
    });

    let xhr = new XMLHttpRequest();
    xhr.open("POST", "/imageDelete");
    xhr.send(formData);
}