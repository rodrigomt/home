;(function () {

    let saleOption          = document.querySelector(".js-sale"),
        rentOption          = document.querySelector(".js-rent"),
        inputTradingOption  = document.querySelector(".js-trading-option");

    saleOption.addEventListener("click", function ()
    {
        saleOption.classList.add("bg-primary");
        saleOption.classList.remove("bg-muted");
        rentOption.classList.add("bg-muted");
        rentOption.classList.remove("bg-primary");
        inputTradingOption.removeAttribute("value")
        inputTradingOption.setAttribute("value", "0");
    });

    rentOption.addEventListener("click", function ()
    {
        saleOption.classList.add("bg-muted");
        saleOption.classList.remove("bg-primary");
        rentOption.classList.add("bg-primary");
        rentOption.classList.remove("bg-muted");
        inputTradingOption.removeAttribute("value");
        inputTradingOption.setAttribute("value", "1");
    });

} ());