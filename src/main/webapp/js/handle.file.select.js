let imageUrl = document.querySelectorAll('.image-url'),
    imageId = document.querySelectorAll('.image-id');

let ListView = function (imageCollection) {
    let images = imageCollection || [];
    let input;

    let _render = function () {
        let listImagesContainer = document.getElementById('list-images'),
            imageNameContainer = document.querySelector(".image-name"),
            cancelNewAd        = document.querySelector(".cancel");

        listImagesContainer.innerHTML = '';
        imageNameContainer.innerHTML = '';

        if(cancelNewAd != null) {
            cancelNewAd.onclick = function(event) {
                images.forEach(function (image) {
                    deleteImage(image.name);
                });
            }
        }

        images.forEach(function (image) {
            let currentImageBox = document.createElement('DIV'),
                currentImage = document.createElement('IMG'),
                buttonRemove = document.createElement('BUTTON');
                input = document.createElement('input');

            currentImageBox.classList.add('list-images-item');
            currentImage.classList.add("thumb");
            currentImage.setAttribute('src', image.src);
            buttonRemove.setAttribute('data-image-id', image.id);

            buttonRemove.textContent = 'X';

            buttonRemove.onclick = function (event) {
                removeImage(parseInt(event.target.getAttribute('data-image-id')));
                deleteImage(image.name);
            };

            currentImageBox.append(currentImage);
            currentImageBox.append(buttonRemove);

            listImagesContainer.appendChild(currentImageBox);

            input.classList.add("image");
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', 'image_name[]');
            input.value = image.name;

            if(image.idImage < 100) {
                imageNameContainer.appendChild(input);
            }
        });

    };

    let removeImage = function (imageId) {
        let index;
        let name;

        images.forEach(function (image, key) {
            if (image.id === imageId) {
                index = key;
            }
        });

        images.splice(index, 1);

        _render();
    };

    let deleteImage = function (name) {

        let xhr = new XMLHttpRequest();
        xhr.open("POST", "/imageDelete");
        xhr.send(`image=${name}`);

    }

    if (imageUrl.length > 0) {

        imageUrl.forEach(function (el, index, array) {
            images.push({
                id: images.length + 1,
                idImage: imageId[index],
                src: el.textContent,
                name: el.textContent.substring(20)
            });
        });
    }

    _render();

    return {
        add: function (name) {
            images.push({
                id: images.length + 1,
                idImage: images.length + 1,
                src: "http://localhost:1000/src/main/webapp/img/" + name,
                name: name
            });
            _render();
        }
    }
};


let listView = new ListView([]);

$('.enter').click(function (event) {
    $('.submit').click();
});

$('.load').click(function (event) {
    $('.files').click();

});

$('#fileupload').fileupload({
    add: function(e, data) {
        data.submit();
    },
    done: function (e, data) {
        $.each(data.files, function (index, file) {

            listView.add(data.result);

        });
    }
});

