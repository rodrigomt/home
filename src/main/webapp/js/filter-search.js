;(function () {

    const allValue       = document.querySelector('.js-all-select'),
          rentValue      = document.querySelector('.js-rent-select'),
          saleValue      = document.querySelector('.js-sale-select'),
          filterAction   = document.querySelector('.filter-action'),
          search         = document.querySelector('.search-local'),
          priceMin       = document.querySelector('.min'),
          priceMax       = document.querySelector('.max'),
          bedroom        = document.querySelector('.bedroom'),
          typeSelection  = document.querySelector('.type-selection'),
          order          = document.querySelector('.order'),
          page           = document.querySelectorAll('.page'),
          btnPrevious    = document.querySelector('.js-previous'),
          btnNext        = document.querySelector('.js-next'),
          url            = document.location;


    let type = typeSelection.value;
    let urlValueCurrent = url.search.substring(url.search.length - 1, url.search.length),
        urlValueOrder   = url.search.substring(7, 8),
        urlPage         = url.search.substring(14, 15),
        option = '';

    console.log(page.length);

    orderState(urlValueOrder);

    if(urlValueCurrent === '1' || urlValueCurrent === '0') {
        option = urlValueCurrent;
    }

    if (urlValueCurrent === '1') {

        rentValue.classList.remove('bg-muted');
        rentValue.classList.add('bg-primary');
        saleValue.classList.remove('bg-primary');
        saleValue.classList.add('bg-muted');

    }

    if(urlValueCurrent === '0'){

        saleValue.classList.remove('bg-muted');
        saleValue.classList.add('bg-primary');
        rentValue.classList.remove('bg-primary');
        rentValue.classList.add('bg-muted');

    }

    if(urlValueCurrent === '=') {
        saleValue.classList.remove('bg-primary');
        saleValue.classList.add('bg-muted');
        rentValue.classList.remove('bg-primary');
        rentValue.classList.add('bg-muted');
        allValue.classList.remove('bg-muted');
        allValue.classList.add('bg-primary');
    }


    allValue.addEventListener('click', function () {
        allValue.classList.add('bg-primary');
        allValue.classList.remove('bg-muted');
        rentValue.classList.remove('bg-primary');
        rentValue.classList.add('bg-muted');
        saleValue.classList.remove('bg-primary');
        saleValue.classList.add('bg-muted');
        type = typeSelection.value;
        option = ''
        optionSelect(order.value, option);
    });

    rentValue.addEventListener('click', function () {
        rentValue.classList.remove('bg-muted');
        rentValue.classList.add('bg-primary');
        saleValue.classList.remove('bg-primary');
        saleValue.classList.add('bg-muted');
        type = typeSelection.value;
        option = '1';
        optionSelect(order.value, option);

    });

    saleValue.addEventListener('click', function() {

        saleValue.classList.remove('bg-muted');
        saleValue.classList.add('bg-primary');
        rentValue.classList.remove('bg-primary');
        rentValue.classList.add('bg-muted');
        type = typeSelection.value;
        option = '0';
       optionSelect(order.value, option);

    });

    search.addEventListener('input', function () {
        search.setAttribute('value', this.value);
    });

    priceMin.addEventListener('input', function () {
        priceMin.setAttribute('value', this.value);
    });

    priceMax.addEventListener('input', function () {
        priceMax.setAttribute('value', this.value)
    });

    bedroom.addEventListener('input', function () {
        bedroom.setAttribute('value', this.value);
    });

    filterAction.addEventListener('click', function() {

        let listAd = document.querySelectorAll('.link-default'),
        type = typeSelection.value,
        searchLocal = search.value,
        pricePrimary = priceMin.value,
        priceSecondary = priceMax.value,
        bedroomValue  = bedroom.value;
        filterType(type,searchLocal, pricePrimary, priceSecondary, bedroomValue, listAd);

    });

    function optionSelect(order, option) {

        if(option === '') {
            document.location = '/adsList?order=' + order + '&page=' + urlPage  + '&search=&option='
        }
        if(option === '0') {
            document.location = '/adsList?order=' + order + '&page=' + urlPage  + '&search=&option=' + option;
        }

        if(option === '1') {
            document.location = '/adsList?order=' + order + '&page=' + urlPage + '&search=&option=' + option;
        }

    }

    function filterType(typeSelection, search, pricePrimary, priceSecondary, bedroomValue, list) {
        for(let i = 0; i < list.length; i++) {

            if(typeSelection !== list.item(i).getAttribute('data-type')) {
                        list.item(i).setAttribute('hidden', '');
            } else {
                if(search !== list.item(i).getAttribute('data-zone')) {
                    list.item(i).setAttribute('hidden', '');
                } else {
                    if (list.item(i).getAttribute('data-price') < pricePrimary || list.item(i).getAttribute('data-price') > priceSecondary) {
                        list.item(i).setAttribute('hidden', '');
                    } else {
                        if(list.item(i).getAttribute('data-bedroom') !== bedroomValue) {
                            list.item(i).setAttribute('hidden', '');
                        } else {
                            list.item(i).removeAttribute('hidden');
                        }
                    }
                }
            }
        }
    }

    order.addEventListener('input', function () {
        optionSelect(order.value, option);
    });

    function orderState(value) {
        let orderAll = document.querySelector('.order-all'),
            orderMin = document.querySelector('.order-min'),
            orderMax = document.querySelector('.order-max');

        if(value === '0') {
            orderAll.setAttribute('selected', '');
            orderMin.removeAttribute('selected');
            orderMax.removeAttribute('selected');
        }

        if(value === '1') {
            orderAll.removeAttribute('selected');
            orderMin.setAttribute('selected', '');
            orderMax.removeAttribute('selected');
        }

        if(value === '2') {
            orderAll.removeAttribute('selected');
            orderMin.removeAttribute('selected');
            orderMax.setAttribute('selected', '');
        }

    }

    btnPrevious.addEventListener('click', function (){
        let pageCurrent = parseInt(urlPage);
        if(pageCurrent > 1) {
            pageCurrent = pageCurrent -1;
        }
        document.location = '/adsList?order=' + urlValueOrder + '&page=' + pageCurrent.toString() + '&search=&option=' + option;
    });

    btnNext.addEventListener('click', function () {
        let pageCurrent = parseInt(urlPage);
        if(pageCurrent < page.length) {
            pageCurrent = pageCurrent + 1;
        }
        document.location = '/adsList?order=' + urlValueOrder + '&page=' + pageCurrent.toString() + '&search=&option=' + option;
    });
} ());