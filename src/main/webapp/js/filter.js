;(function() {

    let allNegotiations         = document.querySelector('.js-all-negotiations'),
        renting                 = document.querySelector('.js-rent-trading'),
        selling                 = document.querySelector('.js-sale-trading');

    let allTypes                = document.querySelector('.js-all-real-estate'),
        house                   = document.querySelector('.js-house'),
        apartment               = document.querySelector('.js-apartment'),
        commercialPoint         = document.querySelector('.js-commercial-point');


    let valueOfAllTrades        = allNegotiations.getAttribute('value'),
        rentingValue            = renting.getAttribute('value'),
        sellingValue            = selling.getAttribute('value');

    let allTypesValue           = allTypes.getAttribute('value'),
        houseValue              = house.getAttribute('value'),
        apartmentValue          = apartment.getAttribute('value'),
        commercialPointValue    = commercialPoint.getAttribute('value');

    let trading                 = '2',
        type                    = '3',
        local                   = new String(document.location).split('?', 1);


    redirectDefault();
    getParameters();
    checkedTrading();
    checkedType();

    allNegotiations.addEventListener('click', function () {
        trading = valueOfAllTrades;
        checkingTypeOfBusiness();
        checkingTypeOfProperty();
        checkingDefaultUrlValue();

    });

    renting.addEventListener('click', function() {
        trading = rentingValue;
        checkingTypeOfBusiness();
        checkingTypeOfProperty();
        buildingUrl()

    });

    selling.addEventListener('click', function () {
        trading = selling;
        checkingTypeOfBusiness();
        checkingTypeOfProperty();
        buildingUrl()

    });

    allTypes.addEventListener('click', function() {
        type = allTypesValue;
        checkingTypeOfBusiness();
        checkingTypeOfProperty();
        checkingDefaultUrlValue()

    });

    house.addEventListener('click', function () {
        type = houseValue;
        checkingTypeOfBusiness();
        checkingTypeOfProperty();
        buildingUrl();

    });

    apartment.addEventListener('click', function () {
        type = apartmentValue;
        checkingTypeOfBusiness();
        checkingTypeOfProperty();
        buildingUrl()

    });

    commercialPoint.addEventListener('click', function () {
        type = commercialPoint;
        checkingTypeOfBusiness();
        checkingTypeOfProperty();
        buildingUrl()

    });

    function checkingTypeOfBusiness() {

        if(allNegotiations.checked) {
            trading = valueOfAllTrades;
        }

        if(selling.checked) {
            trading = sellingValue;
        }

        if(renting.checked) {
            trading = rentingValue;
        }

    }

    function checkingTypeOfProperty() {

        if(allTypes.checked) {
            type = allTypesValue;
        }

        if(apartment.checked) {
            type = apartmentValue;
        }

        if(house.checked) {
            type = houseValue;
        }

        if(commercialPoint.checked) {
            type = commercialPointValue;
        }
    }

    function checkingDefaultUrlValue() {

        if(trading === '2' && type === '3') {

            buildingUrlDefault();

        } else {

            buildingUrl();

        }
    }

    function buildingUrlDefault() {

        document.location = local;
    }

    function buildingUrl () {

        document.location = local + '?trading=' + trading + '&type=' + type;
    }

    function checkedTrading() {

        if (trading === valueOfAllTrades) {
            allNegotiations.setAttribute('checked', 'checked');
            renting.removeAttribute('checked');
            selling.removeAttribute('checked');
        }

        if(trading === rentingValue) {
            renting.setAttribute('checked', 'checked');
            allNegotiations.removeAttribute('checked')
            selling.removeAttribute('checked');
        }

        if(trading === sellingValue) {
            selling.setAttribute('checked', 'checked');
            allNegotiations.removeAttribute('checked');
            renting.removeAttribute('checked');
        }
    }

    function checkedType() {

        if (type === allTypesValue) {
            allTypes.setAttribute('checked', 'checked');
            apartment.removeAttribute('checked');
            house.removeAttribute('checked');
            commercialPoint.removeAttribute('checked');
        }

        if (type === apartmentValue) {
            apartment.setAttribute('checked', 'checked');
            allTypes.removeAttribute('checked');
            house.removeAttribute('checked');
            commercialPoint.removeAttribute('checked');
        }

        if (type === houseValue) {
            house.setAttribute('checked', 'checked');
            allTypes.removeAttribute('checked');
            apartment.removeAttribute('checked');
            commercialPoint.removeAttribute('checked');
        }

        if (type === commercialPointValue) {
            commercialPoint.setAttribute('checked', 'checked');
            allTypes.removeAttribute('checked');
            apartment.removeAttribute('checked');
            house.removeAttribute('checked');
        }
    }

    function getParameters() {

        let query            = location.search.slice(1),
            parameters       = query.split('&');

        parameters.forEach(function (parameter, index, array) {
            let parameterNameValue = parameter.split('='),
                name               = parameterNameValue[0],
                value              = parameterNameValue[1];

            addValue(index, value);
        });
    }

    function addValue(index, value) {

        if(index === 0 ) {

            if(!(value == null) && !(value === '2')) {
                trading = (value);
            } else {
                trading = '2';
            }
        } else {

            if(!(value == null) && !(value === '3')) {
                type = value;
            } else {
                type = '3';
            }
        }
    }

    function redirectDefault() {

        if(String(document.location) === local + '?trading=2&type=3' || String(document.location) === local + '?trading=&type=') {
            document.location = local;
        }
    }

} ());