;(function () {

    let state                   = document.querySelector(".my-state"),
        valueState              = state.getAttribute("value"),
        stateOption             = document.querySelectorAll(".js-state");

    let typeImmobile            = document.querySelector(".my-type-immobile"),
        valueTypeImmobile       = typeImmobile.getAttribute("value"),
        typeImmobileOptions     = document.querySelectorAll(".js-type-immobile");

    let saleOption              = document.querySelector(".js-sale"),
        rentOption              = document.querySelector(".js-rent"),
        inputTradingOption      = document.querySelector(".js-trading-option"),
        valueInput              = inputTradingOption.getAttribute("value");

    stateOption.forEach(function (el, index, array)
    {
        if(el.getAttribute("value") == valueState)
        {
            el.setAttribute("selected", "");
        }
    });

    typeImmobileOptions.forEach(function (el, index, array)
    {
        if(el.getAttribute("value") == valueTypeImmobile)
        {
            el.setAttribute("selected", "");
        }
    });

    if(valueInput == 0)
    {
        saleOption.classList.add("bg-primary");
        saleOption.classList.remove("bg-muted");
        rentOption.classList.add("bg-muted");
        rentOption.classList.remove("bg-primary");
        inputTradingOption.removeAttribute("value")
        inputTradingOption.setAttribute("value", "0");
    }

    if(valueInput == 1)
    {
        saleOption.classList.add("bg-muted");
        saleOption.classList.remove("bg-primary");
        rentOption.classList.add("bg-primary");
        rentOption.classList.remove("bg-muted");
        inputTradingOption.removeAttribute("value");
        inputTradingOption.setAttribute("value", "1");
    }

} ());