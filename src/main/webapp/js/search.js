;(function () {

    let rentValue    = document.querySelector('.js-rent-select');
    let saleValue    = document.querySelector('.js-sale-select');
    let cardOfSearch = document.querySelector('.result-search');
    let searchValue  = '1';

    selectDefault(searchValue);

    rentValue.addEventListener('click', function () {

        let xhr     = new XMLHttpRequest();
        searchValue = '1';

        xhr.open('POST', '/index');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.addEventListener('load', function() {
            if(xhr.status === 200) {
                console.log(xhr.responseText);
                let answer = JSON.parse(xhr.responseText);
                let result = document.querySelector('.result-search');

                result.innerHTML = '';

                for(const ad of answer) {
                    assembleList(ad);
                }
            }
        });
        xhr.send(`trading=${searchValue}`);
    });

    saleValue.addEventListener('click', function() {

        let xhr     = new XMLHttpRequest();
        searchValue = '0';

        xhr.open('POST', '/index');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.addEventListener('load', function() {

            if(xhr.status === 200) {

                let answer  = JSON.parse(xhr.responseText);
                let result  = document.querySelector('.result-search');

                result.innerHTML = '';

                for(const ad of answer) {
                    assembleList(ad);
                }
            }
        });
        xhr.send(`trading=${searchValue}`);
    });

    function assembleList(ad) {
        let div             = document.createElement('div');
        let a               = document.createElement('a');
        let title           = document.createElement('h3');

        a.setAttribute('href', '/adsView?ad_id=' + ad.ID_AD);

        title.classList.add('m-default');
        title.innerText = ad.TITLE;


        a.appendChild(title);
        div.appendChild(a);
        div.classList.add('d-none');
        div.classList.add('div-internal-result');
        div.classList.add('m-auto');
        cardOfSearch.appendChild(div);
    }

    function selectDefault(searchValue) {
        if(searchValue === '1') {

            let xhr = new XMLHttpRequest();

            xhr.open('POST', '/index');
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.addEventListener('load', function() {

                if(xhr.status === 200) {

                    let answer = JSON.parse(xhr.responseText);
                    let result = document.querySelector('.result-search');

                    result.innerHTML = '';

                    for(const ad of answer) {
                        assembleList(ad);
                    }
                }
            });
            xhr.send(`trading=${searchValue}`);
        }
    }
} ());