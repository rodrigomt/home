const buttonLogin = document.querySelector('.js-google');

gapi.load('auth2', () => {
    const that = this,
        client_id = '142522972923-cokl1v3oucv6r5j7cfikapi9bpvrg9t0.apps.googleusercontent.com',
        scope = 'profile email';

    that.auth2 = gapi.auth2.init({
        client_id: client_id,
        scope: scope
    });

    buttonLogin.addEventListener('click', function () {

        that.auth2.signIn().then((response) => {
            const profile = response.getBasicProfile(),
                  auth    = response.getAuthResponse();

            let accessToken = auth.id_token;

            let xhr = new XMLHttpRequest();
            xhr.open('post', '/loginGoogle');

            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onload = function () {
                if(xhr.responseText === 'validated') {
                    document.location = document.location;
                }
            };

            xhr.send(`tokenId=${accessToken}`);

        });
    });

});