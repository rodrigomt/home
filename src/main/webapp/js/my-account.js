;(function () {

    let overlay   = document.querySelector('.overlay'),
        container = document.querySelector('.container'),
        div       = document.getElementById('preload');

    container.setAttribute('hidden', '');
    overlay.removeAttribute('hidden');
    overlay.classList.add('overlay-complements');
    div.classList.add('spinner');

    let xhr     = new XMLHttpRequest();

    xhr.open('POST', '/myAccount');
    xhr.addEventListener('load', function() {

        if(xhr.status === 200) {

            let answer = JSON.parse(xhr.responseText),
                name   = document.getElementById('name-user'),
                email  = document.getElementById('email-user');

            name.textContent = answer.NAME;
            email.textContent = answer.EMAIL;
            overlay.setAttribute('hidden', '');
            container.removeAttribute('hidden');
            div.classList.remove('spinner');
            overlay.classList.remove('overlay-complements');

        }
    });

    xhr.send();

} ());