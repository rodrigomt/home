const btnCollapseHeader = document.querySelector('.js-btn-collapse'),
    navCollapseheader = document.querySelector('.js-navbar-nav-collapse');

btnCollapseHeader.addEventListener('click', function () {
    if (!btnCollapseHeader.classList.contains('navbar-collapse-btn-collapsed')) {
        btnCollapseHeader.classList.add('navbar-collapse-btn-collapsed');
        navCollapseheader.classList.add('navbar-nav-collapse-items-collapsed');
    } else {
        btnCollapseHeader.classList.remove('navbar-collapse-btn-collapsed');
        navCollapseheader.classList.remove('navbar-nav-collapse-items-collapsed');
    }
});