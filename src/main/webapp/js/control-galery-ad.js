;(function () {
    const buttonNext        = document.querySelector(".js-next"),
          buttonPrevious    = document.querySelector(".js-previous");

    let   images = document.querySelectorAll(".js-image"),
          image  = document.querySelector(".js-image-primary"),
          index  = 0;

    if(index == 0)
    {
        images[index].classList.add("border-markup-img");
    }

    buttonNext.addEventListener("click", function(){

        let image = document.querySelector(".js-image-primary");

        if(index <= (images.length - 1))
        {
            if(index == (images.length - 1))
            {
                index = index - 1;
            }

            if (image.getAttribute("src") != images[(index + 1)].getAttribute("src"))
            {

                let urlNew = images[(index + 1)].getAttribute("src");
                image.removeAttribute("src");
                image.setAttribute("src", urlNew);

            }

            index = index + 1;
        }

        selectImage(index);
    });

    buttonPrevious.addEventListener("click", function () {

        let image = document.querySelector(".js-image-primary");

        if(index > 0)
        {

            if(index == images.length)
            {
                index = images.length -1;
            }

            if(image.getAttribute("src") == images[index].getAttribute("src"))
            {

                let urlNew = images[index - 1].getAttribute("src");

                image.removeAttribute("src");
                image.setAttribute("src", urlNew);
            }

            if(index > 0)
            {
                index = index - 1;
            }
        }

        selectImage(index);
    });


    images.forEach(function(el, i, array) {

        el.addEventListener("click", function () {

            if(image.getAttribute("src") != images[i].getAttribute("src")){

                let newUrl = images[i].getAttribute("src");
                image.removeAttribute("src");
                image.setAttribute("src", newUrl);

                index = i;
            }

            selectImage(index);
        });
    });


    function selectImage(index) {

        for (let i = 0; i < images.length; i++){

            if(i == index){
                images[i].classList.add("border-markup-img");
            } else {
                images[i].classList.remove("border-markup-img");
            }
        }
    }
} ());