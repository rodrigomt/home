<%@ page import="br.com.impactante.Home.Interface.Ad.View.Model.Ad" %>
<%@ page import="java.util.List" %>
<%@ page import="br.com.impactante.Home.Infrastructure.Authentication.Session" %>
<%@page contentType="text/html" language="java"%>
<%
    List<br.com.impactante.Home.Interface.Ad.View.Model.Image> images = (List<br.com.impactante.Home.Interface.Ad.View.Model.Image>) request.getAttribute("images");
    Ad ad = (Ad) request.getAttribute("ad");
    br.com.impactante.Home.Interface.Ad.View.Model.Image image = (br.com.impactante.Home.Interface.Ad.View.Model.Image)
            request.getAttribute("image");
%>
<%Session sessionLogin = new Session();%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/util.css">
        <link rel="stylesheet" type="text/css" href="../css/button.css">
        <link rel="stylesheet" type="text/css" href="../css/color.css">
        <link rel="stylesheet" type="text/css" href="../css/form.css">
        <link rel="stylesheet" type="text/css" href="../css/grid.css">
        <link rel="stylesheet" type="text/css" href="../css/modal.css">
        <link rel="stylesheet" type="text/css" href="../css/nav.css">
        <link rel="stylesheet" type="text/css" href="../css/background.css">
        <link rel="shortcut icon" href="../../img/home-logo.png">
        <title>Home</title>
    </head>
    <body>
    <%if (sessionLogin.IsValid(request.getCookies())) {%>
    <div class="navbar">
        <div class="container-fluid">
            <div class="navbar-nav">
                <div class="navbar-nav-element col-4 d-none-sm">
                    <a href="/myAds">Meus anúncios</a>
                </div>
                <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                    <img src="../../img/home-logo.png" width="50">
                </div>
                <div class="navbar-nav-element col-4 text-right d-none-sm position-relative">
                    <a class="btn btn-orange" href="/newAd">Anunciar imóvel</a>
                    <a class="btn btn-profile profile"><%=sessionLogin.getCurrentUserName().substring(0, 2)%></a>
                    <div class="sub-menu position-absolute d-none">
                        <ul>
                            <li><p><%=sessionLogin.getCurrentUserName()%></p></li>
                            <li><a href="#">Minha Conta</a></li>
                            <li><a href="#">Configurações</a></li>
                            <form action="/logout" method="post">
                                <li><input class="btn-logout" type="submit" value="Sair"></li>
                            </form>
                        </ul>
                    </div>
                </div>
                <button class="navbar-collapse-btn js-btn-collapse">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
            <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                <a class="btn btn-orange d-block btn-block" href="/newAd">Anunciar imóvel</a>
            </div>
        </div>
    </div>
    <% } else { %>
    <div class="navbar">
        <div class="container-fluid">
            <div class="navbar-nav">
                <div class="navbar-nav-element col-4 d-none-sm">
                    <a href="/announce">Anunciar imóvel</a>
                </div>
                <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                    <img src="img/home-logo.png" width="50">
                </div>
                <div class="navbar-nav-element col-4 text-right d-none-sm">
                    <a class="btn btn-primary" href="/login">Entrar</a>
                </div>
                <button class="navbar-collapse-btn js-btn-collapse">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
            <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                <a class="btn btn-primary btn-block d-block" href="/login">Entrar</a>
            </div>
        </div>
    </div>
    <%}%>
        <div class="container mt-110">
            <div>
                <a href="#">Home</a> >
                <a href="#">Imóveis</a> >
                <a href="#">Fortaleza</a> >
                <a class="link-actual">Centro</a>
            </div>
            <div class="row mt-20">
                <div class="col-8">
                    <div class="d-flex items-align-center position-relative z-index-1010">
                        <div class="d-flex position-absolute w-100 justify-content-between z-index-1020">
                            <button class="btn-previous js-previous"> < </button>
                            <button class="btn-next js-next"> > </button>
                        </div>
                        <img src="<%=image.url%>" class="img-responsive border-radius js-image-primary">
                        <div class="background-transparent items-align-end position-absolute w-100">
                            <%for (br.com.impactante.Home.Interface.Ad.View.Model.Image img : images){%>
                            <div class="d-inline-block cursor-point"><img src="<%=img.url%>" width="80" class="border-radius-5  js-image"></div>
                            <%}%>
                        </div>
                    </div>

                    <div class="text-justify padding-bottom-40">
                        <small class="text-muted config-info-ad">Rua: <%=ad.street%>,  Nº <%=ad.number%>,  <%=ad.zone%>,  <%=ad.city%> - <%=ad.state%></small>
                        <h1><%=ad.title%></h1>
                        <small class="text-muted config-info-ad">10m² Quartos: 2 suites</small>
                        <p class="text-primary">Mais características</p>
                        <p>
                            <%=ad.description%>
                        </p>
                    </div>
                    <iframe class="mb-4" src="https://www.google.com/maps?q=<%=ad.street%> <%=ad.number%>, <%=ad.zone%>&t=m&z=18&ie=UTF8&iwloc=&output=embed" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
                <div class="col-4">
                    <div class="border m-l-20 p-l-10 p-r-10">
                        <h1 class="border-bottom p-4">Compra</h1>
                        <h2>R$ <%=ad.price%></h2>
                        <div>
                            <small class="text-muted config-info-ad">Condomínio</small>
                            <h6 class="m-default text-size-12">R$ 550,00</h6>
                        </div>
                        <div class="padding-bottom-40">
                            <small class="text-muted config-info-ad">IPTU</small>
                            <h6 class="m-default text-size-12">R$ 1.550,00</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            const btnCollapseHeader = document.querySelector('.js-btn-collapse'),
                  navCollapseheader = document.querySelector('.js-navbar-nav-collapse');

            btnCollapseHeader.addEventListener('click', function () {
                if (!btnCollapseHeader.classList.contains('navbar-collapse-btn-collapsed')) {
                    btnCollapseHeader.classList.add('navbar-collapse-btn-collapsed');
                    navCollapseheader.classList.add('navbar-nav-collapse-items-collapsed');
                } else {
                    btnCollapseHeader.classList.remove('navbar-collapse-btn-collapsed');
                    navCollapseheader.classList.remove('navbar-nav-collapse-items-collapsed');
                }
            });
        </script>
        <script src="../js/control-galery-ad.js"></script>
    <%if (sessionLogin.IsValid(request.getCookies())) {%>
        <script src="../js/menu.perfil.user.js"></script>
    <%}%>
    </body>
</html>