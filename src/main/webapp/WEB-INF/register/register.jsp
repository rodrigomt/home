<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../css/util.css">
        <link rel="stylesheet" type="text/css" href="../../css/button.css">
        <link rel="stylesheet" type="text/css" href="../../css/color.css">
        <link rel="stylesheet" type="text/css" href="../../css/form.css">
        <link rel="stylesheet" type="text/css" href="../../css/grid.css">
        <link rel="stylesheet" type="text/css" href="../../css/login.css">
        <link rel="shortcut icon" href="../../img/home-logo.png">
        <title>Home</title>
    </head>
    <body>
        <div class="form-login w-30 p-3">
            <div class="text-center">
                <img src="../../img/home-logo.png" width="80">
            </div>
            <div class="d-flex flex-column mt-3">
                <div class="mt-3">
                    <button class="btn btn-danger w-100">Google</button>
                </div>
                <div class="mt-3 p-3 d-flex justify-content-between">
                    <hr class="w-20 h"/>
                    <span class="w-60 text-muted text-center">Ou entre com seu e-mail</span>
                    <hr class="w-20 h"/>
                </div>
                <div>
                    <form action="/register" method="post" enctype="application/x-www-form-urlencoded">
                        <input class="w-100 mt-3" name="name" type="text" placeholder="Nome" required>
                        <input class="w-100 mt-1" name="email" type="email" placeholder="E-mail" required>
                        <input class="w-100 mt-1" name="password" type="password" placeholder="Senha" required>
                        <button class="btn btn-primary w-100 mt-3">Cadastrar</button>
                    </form>
                </div>
                <div class="d-flex justify-content-between mt-3">
                    <a href="/recoverPassword">Recuperar senha</a>
                    <a href="/login">Entrar</a>
                </div>
            </div>
        </div>
    </body>
</html>