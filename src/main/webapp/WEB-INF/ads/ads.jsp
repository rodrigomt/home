<%@ page import="java.util.List" %>
<%@ page import="br.com.impactante.Home.Interface.Ad.View.Model.Ad" %>
<%@ page import="br.com.impactante.Home.Interface.Ad.View.Model.Image" %>
<%@ page import="br.com.impactante.Home.Infrastructure.Authentication.Session" %>
<%@ page contentType="text/html" language="java" %>
<%
    List<Ad> list = (List<Ad>) request.getAttribute("list");
    int numberPages = (int) request.getAttribute("numberPages");
    int pag         = (int) request.getAttribute("page");
    String linkOne  = (String) request.getAttribute("linkOne");
    String linkTwo  = (String) request.getAttribute("linkTwo");
    Session sessionLogin = new Session();
%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../css/util.css">
        <link rel="stylesheet" type="text/css" href="../../css/button.css">
        <link rel="stylesheet" type="text/css" href="../../css/color.css">
        <link rel="stylesheet" type="text/css" href="../../css/form.css">
        <link rel="stylesheet" type="text/css" href="../../css/grid.css">
        <link rel="stylesheet" type="text/css" href="../../css/modal.css">
        <link rel="stylesheet" type="text/css" href="../../css/nav.css">
        <link rel="stylesheet" type="text/css" href="../../css/background.css">
        <link rel="shortcut icon" href="../../img/home-logo.png">
        <title>Home</title>
    </head>
    <body>
        <%if (sessionLogin.IsValid(request.getCookies())) {%>
        <div class="navbar">
            <div class="container-fluid">
                <div class="navbar-nav">
                    <div class="navbar-nav-element col-4 d-none-sm">
                        <a href="/myAds">Meus anúncios</a>
                    </div>
                    <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                        <img src="../../img/home-logo.png" width="50">
                    </div>
                    <div class="navbar-nav-element col-4 text-right d-none-sm position-relative">
                        <a class="btn btn-orange" href="/newAd">Anunciar imóvel</a>
                        <a class="btn btn-profile profile"><%=sessionLogin.getCurrentUserName().substring(0, 2)%></a>
                        <div class="sub-menu position-absolute d-none">
                            <ul>
                                <li><p><%=sessionLogin.getCurrentUserName()%></p></li>
                                <li><a href="/myAccount">Minha Conta</a></li>
                                <li><a href="/configs">Configurações</a></li>
                                <form action="/logout" method="post">
                                    <li><input class="btn-logout" type="submit" value="Sair"></li>
                                </form>
                            </ul>
                        </div>
                    </div>
                    <button class="navbar-collapse-btn js-btn-collapse">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                    <a class="btn btn-orange d-block btn-block" href="/newAd">Anunciar imóvel</a>
                </div>
            </div>
        </div>
        <%} else {%>
        <div class="navbar">
            <div class="container-fluid">
                <div class="navbar-nav">
                    <div class="navbar-nav-element col-4 d-none-sm">
                        <a href="/announce">Anunciar imóvel</a>
                    </div>
                    <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                        <img src="../../img/home-logo.png" width="50">
                    </div>
                    <div class="navbar-nav-element col-4 text-right d-none-sm">
                        <a class="btn btn-primary" href="/login">Entrar</a>
                    </div>
                    <button class="navbar-collapse-btn js-btn-collapse">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                    <a class="btn btn-primary btn-block d-block" href="/login">Entrar</a>
                </div>
            </div>
        </div>
        <%}%>
        <div class="container mt-80">
            <div class="row mt-30">
                <div class="col-3 position-fixed position-relative-m p-40-m">
                    <div  class="background-color-filter p-3 mr-3 border-radius mt-40 text-white m-default-m">
                        <h6 class="mb-5">Qual tipo?</h6>
                        <select name="type" class="type-selection w-100">
                            <option value="0">Apartamento</option>
                            <option value="1">Casa</option>
                        </select>

                        <h6 class="mb-5">Onde Fica?</h6>
                        <input class="search-local w-100" type="text">

                        <h6 class="mb-5">Preço:</h6>
                        <input class="min w-35" type="text" placeholder="Min">
                        <span class="padding-personal">Até</span>
                        <input class="max w-35" type="text" placeholder="Max">

                        <h6 class="mb-5">Quantos quartos?</h6>
                        <select class="bedroom">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                        <button class="btn btn-orange mt-2 w-100 d-inline-block filter-action">Filtrar</button>
                    </div>
                </div>
                <div class="col-9 ml-35 m-default-m">
                    <div class="d-flex items-align-center justify-content-between border-bottom p-3 box-mobile">
                        <div class="w-40 box-internal-mobile">
                            <button type="button" class="btn bg-muted js-all-select">Todos</button>
                            <button type="button" class="btn bg-primary js-rent-select">Alugar</button>
                            <button type="button" class="btn bg-muted js-sale-select">Comprar</button>
                        </div>
                        <div class="w-30 align-text box-internal-mobile">
                            <select class="order">
                                <option class="order-all" value="0" selected>Ordenar por</option>
                                <option class="order-min" value="1">Menor Preço</option>
                                <option class="order-max" value="2">Maior Preço</option>
                            </select>
                        </div>
                    </div>
                    <div class="p-20-m">
                        <h4 class="text-muted mb-default">Imóveis localizados: 50, em Fortaleza.</h4>
                        <%for(Ad ad : list){%>
                        <a class="link-default " data-type="<%=ad.typeImmobile%>" data-state="<%=ad.state%>" data-zone="<%=ad.zone%>" data-city="<%=ad.city%>" data-street="<%=ad.street%>" data-price="<%=ad.price%>" data-bedroom="<%=ad.bedroom%>" href="/adsView?ad_id=<%=ad.id%>">

                            <div class="row items-align-center border-bottom pt-3 pb-3">
                                <div class="col-5 text-center p-7">
                                    <%if(ad.imageDate.size() == 0){%>
                                    <img clas="img-responsive" src="../../img/no-photo.jpeg">
                                    <%} else {%>
                                    <img class="img-responsive" src="<%=ad.imageDate.get(0).getUrl()%>">
                                    <%}%>
                                </div>
                                <div class="col-7 p-7">
                                    <p hidden><%=ad.typeImmobile%></p>
                                    <small class="text-muted">Rua: <%=ad.street%>, Nº <%=ad.number%>, <%=ad.zone%></small>
                                    <h3 class="text-secondary mb-default mt-default mt-1"><%=ad.title%></h3>
                                    <small class="d-inline-block text-muted border-opt b-rds mt-1 p-opt">quartos: <%=ad.bedroom%></small>
                                    <small class="d-inline-block text-muted border-opt b-rds mt-1 p-opt">suites :<%=ad.suite%></small>
                                    <small class="d-inline-block text-muted border-opt b-rds mt-1 p-opt"><%=ad.size%>m²</small>
                                    <p class="text-secondary">
                                        <%if(ad.description.length() > 100) {%>
                                            <%=ad.description.substring(0, 100) + "..."%>
                                        <%} else {%>
                                            <%=ad.description%>
                                        <%}%>

                                    </p>
                                    <div class="background-color-buy p-7 text-center text-white">
                                        <h1 class="m-default">R$ <%=ad.price%></h1>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <%}%>
                    </div>
                    <%if(numberPages > 1) {%>
                    <div class="pagination d-flex items-align-center padding-20 float-right">
                        <a class="js-previous" href="#"><img src="../../img/previous.png"></a>
                        <%for(int i = 0; i < numberPages; i++) {%>
                        <a href="<%=linkOne + (i + 1) + linkTwo%>" class="page"><span class="d-inline-block p-10 <%if (pag == i + 1){%>bg-primary text-white border-radius-5<%}%>"><%=i + 1%></span></a>
                        <%}%>
                        <a class="js-next" href="#"><img src="../../img/next.png"></a>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
        <script src="../../js/mobile.js"></script>
        <script src="../../js/filter-search.js"></script>
        <script src="../../js/menu.perfil.user.js"></script>
    </body>
</html>