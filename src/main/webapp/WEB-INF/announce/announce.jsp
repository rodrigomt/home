<%@ page contentType="text/html" language="java" %>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../css/util.css">
        <link rel="stylesheet" type="text/css" href="../../css/button.css">
        <link rel="stylesheet" type="text/css" href="../../css/form.css">
        <link rel="stylesheet" type="text/css" href="../../css/color.css">
        <link rel="stylesheet" type="text/css" href="../../css/grid.css">
        <link rel="stylesheet" type="text/css" href="../../css/nav.css">
        <link rel="stylesheet" type="text/css" href="../../css/background.css">
        <link rel="shortcut icon" href="../../img/home-logo.png">
        <title>Anunciar imovel</title>
    </head>
    <body>
        <div class="navbar">
            <div class="container-fluid">
                <div class="navbar-nav">
                    <div class="navbar-nav-element col-4 d-none-sm">
                        <a href="/">Volta para pagina principal</a>
                    </div>
                    <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                        <img src="img/home-logo.png" width="50">
                    </div>
                    <div class="navbar-nav-element col-4 text-right d-none-sm">
                        <a class="btn btn-primary" href="/login">Entrar</a>
                    </div>
                    <button class="navbar-collapse-btn js-btn-collapse">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                    <a class="btn btn-primary btn-block d-block" href="/login">Entrar</a>
                </div>
            </div>
        </div>
        <div class="container-fluid h-vh background-img d-flex items-align-center justify-content-between">
            <div class="container mt-150">
                <div class="w-100 p-3 d-flex flex-column mt-3">
                    <div class="w-100 mt-3 d-block bg-white m-auto text-center box-sh b-rds">
                        <div class="d-inline-block text-left w-50 p-3 m-25 position-relative float-left">
                            <h1 class="mt-default text-size-30 text-muted">Querendo anunciar seu imovel?</h1>
                            <h3 class="text-muted text-justify">Cadastre-se em nosso sistema e experimente
                            durante 10 dias, se gosta, selecione o plano de sua preferencia e continue anunciando
                            seu imovel.</h3>

                        </div>
                        <div class="d-inline-block w-40 text-center p-3 m-25">
                            <div class="mt-3">
                                <button class="btn btn-danger w-100">Google</button>
                            </div>
                            <div class="mt-3 p-3 d-flex justify-content-between">
                                <hr class="w-20 h"/>
                                <span class="w-60 text-muted text-center">Ou entre com seu e-mail</span>
                                <hr class="w-20 h"/>
                            </div>
                            <div>
                                <form action="/register" method="post" enctype="application/x-www-form-urlencoded">
                                    <input class="w-100 mt-3" name="name" type="text" placeholder="Nome" required>
                                    <input class="w-100 mt-1" name="email" type="email" placeholder="E-mail" required>
                                    <input class="w-100 mt-1" name="password" type="password" placeholder="Senha" required>
                                    <input name="uri" type="hidden" value="/announce">
                                    <button class="btn btn-primary w-100 mt-3">Cadastrar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../js/mobile.js"></script>
    </body>
</html>
