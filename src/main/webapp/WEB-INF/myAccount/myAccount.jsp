<%@ page import="br.com.impactante.Home.Infrastructure.Authentication.Session" %>
<%@ page import="br.com.impactante.Home.Domain.Account.User" %>
<%@ page contentType="text/html" language="java" %>
<%Session sessionLogin = new Session();%>
<%if (sessionLogin.IsValid(request.getCookies())) {%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../css/util.css">
    <link rel="stylesheet" type="text/css" href="../../css/button.css">
    <link rel="stylesheet" type="text/css" href="../../css/color.css">
    <link rel="stylesheet" type="text/css" href="../../css/form.css">
    <link rel="stylesheet" type="text/css" href="../../css/grid.css">
    <link rel="stylesheet" type="text/css" href="../../css/modal.css">
    <link rel="stylesheet" type="text/css" href="../../css/nav.css">
    <link rel="stylesheet" type="text/css" href="../../css/background.css">
    <link rel="stylesheet" type="text/css" href="../../css/form-ad.css">
    <link rel="shortcut icon" href="../../img/home-logo.png">
    <title>Home</title>
</head>
<body>
<div class="overlay w-100" hidden>
    <div id="preload"></div>
</div>
<div class="navbar">
    <div class="container-fluid">
        <div class="navbar-nav">
            <div class="navbar-nav-element col-4 d-none-sm">
                <a href="/myAds">Meus anúncios</a>
            </div>
            <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                <a href="/"><img src="../../img/home-logo.png" width="50"></a>
            </div>
            <div class="navbar-nav-element col-4 text-right d-none-sm position-relative">
                <a class="btn btn-orange" href="/newAd">Anunciar imóvel</a>
                <a class="profile btn btn-profile"><%=sessionLogin.getCurrentUserName().substring(0, 2)%></a>
                <div class="sub-menu position-absolute d-none">
                    <ul>
                        <li><p><%=sessionLogin.getCurrentUserName()%></p></li>
                        <li><a href="/myAccount">Minha Conta</a></li>
                        <li><a href="/configs">Configurações</a></li>
                        <form action="/logout" method="post">
                            <li><input class="btn-logout" type="submit" value="Sair"></li>
                        </form>
                    </ul>
                </div>
            </div>

            <button class="navbar-collapse-btn js-btn-collapse">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>
        <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
            <a class="btn btn-orange d-block btn-block" href="/newAd">Anunciar imóvel</a>
            <a class="btn btn-primary d-block btn-block mt-2" href="/myAds">Meus Anúncios</a>
        </div>
    </div>
</div>
<div class="container-fluid container-fluid-m h-vh d-flex items-align-center justify-content-between">
    <div class="container mt-80 position-relative">
        <div class="create-ad w-100 p-3 d-flex flex-column items-align-center">
            <div class="p-3 w-50 w-100-m">
                <div class="card">
                    <div class="card-header">
                        <h1 class="m-default">Informaçoes da conta</h1>
                    </div>
                    <div class="card-body">
                        <span class="name-data">Nome:</span>
                        <span id="name-user"></span><br>
                        <span class="name-data">Email:</span>
                        <span id="email-user"></span><br>
                        <span class="name-data">Telefone:</span>
                        <span id="fone-user">(85) 98822-1293</span><br>
                        <span class="name-data">quantidade de anuncios:</span>
                        <span>3</span>
                    </div>
                </div>
            </div>
            <div class="d-inline-block options p-3 w-100-m">
                <a class="btn btn-primary edit-data d-block-m mt-2-m" data="edit-data">Editar dados da conta</a>
                <a class="btn btn-orange update-password d-block-m mt-2-m" data="edit-password">Trocar senha</a>
                <a class="btn btn-danger delete-account d-block-m mt-2-m" data="delete">Excluir conta</a>
            </div>
        </div>
        <dialog class="dialog w-90-m m-auto-m mt-500-m">

        </dialog>
    </div>
</div>
<script src="../../js/mobile.js"></script>
<script src="../../js/menu.perfil.user.js"></script>
<script src="../../js/modals.js"></script>
<script src="../../js/my-account.js"></script>
</body>
</html>
<%}%>