<%@ page import="java.util.List" %>
<%@ page import="br.com.impactante.Home.Interface.Ad.View.Model.Ad" %>
<%@ page import="br.com.impactante.Home.Domain.Ad.Image" %>
<%@ page import="br.com.impactante.Home.Infrastructure.Authentication.Session" %>
<%@ page import="br.com.impactante.Home.Pagination.Pagination" %>
<%@ page import="br.com.impactante.Home.Application.AdApplication" %>
<%@ page import="br.com.impactante.Home.Application.AdApplicationFactory" %>
<%@ page contentType="text/html" language="java" %>
<%
    int pageCurrent;

    List<Ad> list = (List<Ad>) request.getAttribute("list");
    Session sessionLogin = new Session();

    if(request.getParameter("page") != null)
    {
       pageCurrent = Integer.parseInt(request.getParameter("page"));
    }
%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../css/util.css">
    <link rel="stylesheet" type="text/css" href="../../css/button.css">
    <link rel="stylesheet" type="text/css" href="../../css/color.css">
    <link rel="stylesheet" type="text/css" href="../../css/form.css">
    <link rel="stylesheet" type="text/css" href="../../css/grid.css">
    <link rel="stylesheet" type="text/css" href="../../css/modal.css">
    <link rel="stylesheet" type="text/css" href="../../css/nav.css">
    <link rel="stylesheet" type="text/css" href="../../css/background.css">
    <link rel="shortcut icon" href="../../img/home-logo.png">
    <title>Home</title>
</head>
<body>
    <%if (sessionLogin.IsValid(request.getCookies())) {%>
    <div class="navbar">
        <div class="container-fluid">
            <div class="navbar-nav">
                <div class="navbar-nav-element col-4 d-none-sm">
                    <a href="/">Pagina Principal</a>
                </div>
                <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                    <a href="/"><img src="../../img/home-logo.png" width="50"></a>
                </div>
                <div class="navbar-nav-element col-4 text-right d-none-sm position-relative">
                    <a class="btn btn-orange" href="/newAd">Anunciar imóvel</a>
                    <a class="btn btn-profile profile"><%=sessionLogin.getCurrentUserName().substring(0, 2)%></a>
                    <div class="sub-menu position-absolute d-none">
                        <ul>
                            <li><p><%=sessionLogin.getCurrentUserName()%></p></li>
                            <li><a href="/myAccount">Minha Conta</a></li>
                            <li><a href="/configs">Configurações</a></li>
                            <form action="/logout" method="post">
                                <li><input class="btn-logout" type="submit" value="Sair"></li>
                            </form>
                        </ul>
                    </div>
                </div>
                <%}%>
                <button class="navbar-collapse-btn js-btn-collapse">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
            <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                <a class="btn btn-orange d-block btn-block" href="/newAd">Anunciar imóvel</a>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-80">
        <div class="row mt-30">
            <div class="col-3 padding-20 mt-20 position-fixed position-relative-m">
                <h2 hidden>Filtros</h2>
                <div  class="p-3 mr-3 bg-primary text-white filter-trading">
                    <h3 class="mb-5">Negociação</h3>
                    <div class="d-flex items-align-center">
                        <input class="border-none js-all-negotiations" type="radio" name="negotiation" value="2">
                        <label>Todos</label>
                    </div>
                    <div class="d-flex items-align-center mt-20-negative">
                        <input class="border-none js-rent-trading" type="radio" name="negotiation" value="1">
                        <label>Alugados</label>
                    </div>
                    <div class="d-flex items-align-center mt-20-negative">
                        <input class="border-none js-sale-trading" type="radio" name="negotiation" value="0">
                        <label>Vendas</label>
                    </div>
                </div>

                <div  class="p-3 mr-3 mt-20 bg-primary text-white filter-type">
                    <h3 class="mb-5">Tipo de imóvel</h3>
                    <div class="d-flex items-align-center">
                        <input class="border-none js-all-real-estate" type="radio" name="type" value="3">
                        <label>Todos</label>
                    </div>
                    <div class="d-flex items-align-center mt-20-negative">
                        <input class="border-none js-apartment" type="radio" name="type" value="0">
                        <label>Apartamento</label>
                    </div>
                    <div class="d-flex items-align-center mt-20-negative">
                        <input class="border-none js-house" type="radio" name="type" value="1">
                        <label>casa</label>
                    </div>
                    <div class="d-flex items-align-center mt-20-negative">
                        <input class="border-none js-commercial-point" type="radio" name="type" value="2">
                        <label>Ponto Comercial</label>
                    </div>
                </div>
            </div>
            <div class="col-9 padding-20 ml-25 ml-default-m">
                <div class="mt-20" id="lst">
                    <%if(list == null){%>
                    <div class="row items-align-center border-top p-10">
                        <h2>Sem Resultados</h2>
                    </div>
                    <%} else {%>
                    <%for(Ad ad : list){%>
                    <div class="row items-align-center border-top p-10">
                        <%if(ad.imageDate.size() == 0){%>
                            <img clas="img-responsive" src="../../img/no-photo.jpeg">
                        <%} else {%>
                            <div class="col-3 text-center p-7">
                                <img class="img-responsive" src="<%=ad.imageDate.get(0).getUrl()%>">
                            </div>
                        <%}%>
                        <div class="col-9 p-7">
                            <small class="text-muted"><%=ad.street%>, Nº <%=ad.number%>, <%=ad.zone%></small>
                            <h3 class="text-secondary mb-default mt-default mt-1"><%=ad.title%></h3>
                            <small class="d-inline-block text-muted border-opt b-rds mt-1 p-opt">Quartos: <%=ad.bedroom%></small>
                            <small class="d-inline-block text-muted border-opt b-rds mt-1 p-opt">Suite: <%=ad.suite%></small>
                            <small class="d-inline-block text-muted border-opt b-rds mt-1 p-opt">Banheiro: <%=ad.bathroom%></small>
                            <small class="d-inline-block text-muted border-opt b-rds mt-1 p-opt"><%=ad.size%>m²</small>
                            <p class="text-secondary">
                                <%if(ad.description.length() > 100) {%>
                                    <%=ad.description.substring(0, 100) + "..."%>
                                <%} else {%>
                                    <%=ad.description%>
                                <%}%>
                            </p>
                            <div class="align-text p-10">
                                <%for(Image image : ad.imageDate){%>
                                    <spam class="d-none image-url"><%=image.getUrl().substring(20)%></spam>
                                <%}%>
                                <button class="btn btn-danger js-delete" id="<%=ad.id%>">Excluir</button>
                                <a href="/updateAd?id=<%=ad.id%>" class="btn btn-primary">Editar</a>
                            </div>
                        </div>
                    </div>
                    <%}%>
                    <%}%>
                </div>

<%--                <div class="pages">--%>
<%--                    <%if(pagination.hasPrevious()){%>--%>
<%--                    <div class="min-box box-page">--%>
<%--                        <a href="/agenda/?page=<%=pagination.getPreviousNumberPage()%>"> <img class="width" src="img/left.png"> </a>--%>
<%--                    </div>--%>
<%--                    <% } %>--%>

<%--                    <%for(int i = 1; i <= pagination.getNumberPages(); i++){%>--%>
<%--                    <div class="min-box box-page <%if(i == pageCurrent){%>selected<%}%>">--%>
<%--                        <a <%if(i != pageCurrent){%> href="/agenda/?page=<%=i%>"<%}%>><%=i%></a>--%>
<%--                    </div>--%>
<%--                    <%}%>--%>

<%--                    <%if(pagination.hasNext()){%>--%>
<%--                    <div class="min-box box-page">--%>
<%--                        <a href="/agenda/?page=<%=pagination.getNextNumberPage()%>"> <img class="width" src="img/right.png"> </a>--%>
<%--                    </div>--%>
<%--                    <%}%>--%>

<%--                    <%}%>--%>
<%--                </div>--%>

            </div>
        </div>
    </div>
    <script src="../../js/mobile.js"></script>
    <script src="../../js/filter.js"></script>
    <script src="../../js/delete.ad.js"></script>
    <script src="../../js/menu.perfil.user.js"></script>
</body>
</html>