<%@ page import="br.com.impactante.Home.Infrastructure.Authentication.Session" %>
<%@ page contentType="text/html" language="java" %>
<%Session sessionLogin = new Session();%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../css/util.css">
        <link rel="stylesheet" type="text/css" href="../../css/button.css">
        <link rel="stylesheet" type="text/css" href="../../css/color.css">
        <link rel="stylesheet" type="text/css" href="../../css/form.css">
        <link rel="stylesheet" type="text/css" href="../../css/grid.css">
        <link rel="stylesheet" type="text/css" href="../../css/modal.css">
        <link rel="stylesheet" type="text/css" href="../../css/nav.css">
        <link rel="stylesheet" type="text/css" href="../../css/background.css">
        <link rel="stylesheet" type="text/css" href="../../css/form-ad.css">
        <link rel="shortcut icon" href="../../img/home-logo.png">
        <title>Home</title>
    </head>
    <body>
    <%if (sessionLogin.IsValid(request.getCookies())) {%>
    <div class="navbar">
        <div class="container-fluid">
            <div class="navbar-nav">
                <div class="navbar-nav-element col-4 d-none-sm">
                    <a href="/myAds">Meus anúncios</a>
                </div>
                <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                    <a href="/"><img src="<%=request.getContextPath()%>/img/home-logo.png" width="50"></a>
                </div>
                <div class="navbar-nav-element col-4 text-right d-none-sm position-relative">
                    <a class="btn btn-orange" href="/newAd">Anunciar imóvel</a>
                    <a class="profile btn btn-profile"><%=sessionLogin.getCurrentUserName().substring(0, 2)%></a>
                    <div class="sub-menu position-absolute d-none">
                        <ul>
                            <li><p><%=sessionLogin.getCurrentUserName()%></p></li>
                            <li><a href="/myAccount">Minha Conta</a></li>
                            <li><a href="/configs">Configurações</a></li>
                            <form action="/logout" method="post">
                                <li><input class="btn-logout" type="submit" value="Sair"></li>
                            </form>
                        </ul>
                    </div>
                </div>
                <%}%>
                <button class="navbar-collapse-btn js-btn-collapse">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
            <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                <a class="btn btn-profile">RO</a>
                <a class="btn btn-orange d-block btn-block" href="/newAd">Anunciar imóvel</a>
                <a class="btn btn-primary d-block btn-block mt-2" href="/myAds">Meus Anúncios</a>
            </div>
        </div>
    </div>
    <div class="container-fluid h-vh d-flex items-align-center justify-content-between">
        <div class="container">
            <div class="create-ad w-100 p-3 d-flex flex-column">
                <div class="page-new-ad">
                    <h1 class="page-new-ad-header">Configurações da conta</h1>
                    <div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../js/menu.perfil.user.js"></script>
    </body>
</html>
