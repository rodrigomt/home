<%@ page contentType="text/html" language="java" %>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../css/util.css">
        <link rel="stylesheet" type="text/css" href="../../css/button.css">
        <link rel="stylesheet" type="text/css" href="../../css/color.css">
        <link rel="stylesheet" type="text/css" href="../../css/grid.css">
        <link rel="stylesheet" type="text/css" href="../../css/nav.css">
        <link rel="stylesheet" type="text/css" href="../../css/background.css">
        <link rel="shortcut icon" href="../../img/home-logo.png">
        <title>404 - Pagina nao encontrada</title>
    </head>
    <body>
        <div class="navbar">
            <div class="container-fluid">
                <div class="navbar-nav">
                    <div class="navbar-nav-element col-4 d-none-sm">
                        <a href="/announce">Anunciar imóvel</a>
                    </div>
                    <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                        <img src="img/home-logo.png" width="50">
                    </div>
                    <div class="navbar-nav-element col-4 text-right d-none-sm">
                        <a class="btn btn-primary" href="/login">Entrar</a>
                    </div>
                    <button class="navbar-collapse-btn js-btn-collapse">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                    <a class="btn btn-primary btn-block d-block" href="/login">Entrar</a>
                </div>
            </div>
        </div>
        <div class="container-fluid h-vh background-img d-flex items-align-center justify-content-between">
            <div class="container">
                <div class="w-100 p-3 d-flex flex-column mt-3">
                    <h2 class="mt-default mb-default text-size text-shadow text-center text-white">404</h2>
                    <h1 class="mt-default mb-default text-size text-shadow text-center text-white">Pagina nao encontrada.</h1>
                    <div class="w-60 bg-white m-auto mt-3 text-center box-sh b-rds p-3">
                        <a href="/">--- Ir para pagina principal ---</a>
                    </div>
                </div>
            </div>
        </div>
        <script>
            const btnCollapseHeader = document.querySelector('.js-btn-collapse'),
                navCollapseheader = document.querySelector('.js-navbar-nav-collapse');

            btnCollapseHeader.addEventListener('click', function () {
                if (!btnCollapseHeader.classList.contains('navbar-collapse-btn-collapsed')) {
                    btnCollapseHeader.classList.add('navbar-collapse-btn-collapsed');
                    navCollapseheader.classList.add('navbar-nav-collapse-items-collapsed');
                } else {
                    btnCollapseHeader.classList.remove('navbar-collapse-btn-collapsed');
                    navCollapseheader.classList.remove('navbar-nav-collapse-items-collapsed');
                }
            });
        </script>
    </body>
</html>
