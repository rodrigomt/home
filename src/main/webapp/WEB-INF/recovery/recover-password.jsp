<%@ page contentType="text/html;" language="java" %>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../css/util.css">
    <link rel="stylesheet" type="text/css" href="../../css/button.css">
    <link rel="stylesheet" type="text/css" href="../../css/color.css">
    <link rel="stylesheet" type="text/css" href="../../css/form.css">
    <link rel="stylesheet" type="text/css" href="../../css/grid.css">
    <link rel="stylesheet" type="text/css" href="../../css/login.css">
    <link rel="shortcut icon" href="../../img/home-logo.png">
    <title>Home</title>
</head>
<body>
    <div class="overlay w-100" hidden>
        <div id="preload"></div>
    </div>
    <div class="form-login w-30 p-3">
        <div class="text-center">
            <img src="../../img/home-logo.png" width="80">
        </div>
        <div class="d-flex flex-column mt-3">
            <div class="mt-3 p-3 d-flex justify-content-between">
                <hr class="w-20 h"/>
                <span class="w-60 text-muted text-center text-info">Digite o e-mail cadastrado</span>
                <hr class="w-20 h"/>
            </div>
            <div class="box-alert" hidden>
                <p class="alert-recover msg" hidden></p>
            </div>
            <div>
                <form class="form-recover">
                    <input class="w-100 mt-3 email" type="email" placeholder="E-mail" required>
                    <button type="button" class="btn btn-primary w-100 mt-3 btn-send">Enviar</button>
                </form>
                <form class="form-code" hidden>
                    <input class="w-100 mt-3 code" type="text" placeholder="Código de verificação" required>
                    <button type="button" class="btn btn-primary w-100 mt-3 btn-send-code">Enviar</button>
                </form>
                <form class="form-password" hidden>
                    <input class="w-100 mt-3 new-password" type="password" placeholder="Nova senha" required>
                    <input class="w-100 mt-3 confirm-new-password" type="password" placeholder="Confirme nova senha" required>
                    <button type="button" class="btn btn-primary w-100 mt-3 btn-send-new-password">Enviar</button>
                </form>
                <div class="success bg-green text-center p-10 b-rds" hidden>
                    <h1 class="text-green m-default">Senha alterada</h1>
                    <a href="/login">Faça o login aqui</a>
                </div>
            </div>
            <div class="d-flex justify-content-between mt-3">
                <a href="/">&larr; Voltar</a>
            </div>
        </div>
    </div>
    <script src="../../js/recovery-password.js"></script>
    <script src="../../js/code-verification.js"></script>
    <script src="../../js/password-update.js"></script>
</body>
</html>