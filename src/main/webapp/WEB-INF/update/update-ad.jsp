<%@ page import="br.com.impactante.Home.Interface.Ad.View.Model.Ad" %>
<%@ page import="br.com.impactante.Home.Domain.Ad.Image" %>
<%@ page import="br.com.impactante.Home.Infrastructure.Authentication.Session" %>
<%@ page contentType="text/html" language="java" %>
<%Session sessionLogin = new Session();%>
<%Ad ad = (Ad) request.getAttribute("ad");%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../../css/util.css">
        <link rel="stylesheet" type="text/css" href="../../css/button.css">
        <link rel="stylesheet" type="text/css" href="../../css/color.css">
        <link rel="stylesheet" type="text/css" href="../../css/form.css">
        <link rel="stylesheet" type="text/css" href="../../css/grid.css">
        <link rel="stylesheet" type="text/css" href="../../css/modal.css">
        <link rel="stylesheet" type="text/css" href="../../css/nav.css">
        <link rel="stylesheet" type="text/css" href="../../css/background.css">
        <link rel="stylesheet" type="text/css" href="../../css/form-ad.css">
        <link rel="shortcut icon" href="../../img/home-logo.png">
        <title>Home</title>
    </head>
    <body class="bg-snow">
        <div class="overlay w-100" hidden>
            <div id="preload"></div>
        </div>
        <%if (sessionLogin.IsValid(request.getCookies())) {%>
        <div class="navbar">
            <div class="container-fluid">
                <div class="navbar-nav">
                    <div class="navbar-nav-element col-4 d-none-sm">
                        <a href="/myAds">Meus anúncios</a>
                    </div>
                    <div class="navbar-nav-element col-4 text-center navbar-header-logo">
                        <img src="../../img/home-logo.png" width="50">
                    </div>
                    <div class="navbar-nav-element col-4 text-right d-none-sm position-relative">
                        <a class="btn btn-orange" href="/">Pagina Principal</a>
                        <a class="profile btn btn-profile"><%=sessionLogin.getCurrentUserName().substring(0, 2)%></a>
                        <div class="sub-menu position-absolute d-none">
                            <ul>
                                <li><p><%=sessionLogin.getCurrentUserName()%></p></li>
                                <li><a href="/myAccount">Minha Conta</a></li>
                                <li><a href="/configs">Configurações</a></li>
                                <form action="/logout" method="post">
                                    <li><input class="btn-logout" type="submit" value="Sair"></li>
                                </form>
                            </ul>
                        </div>
                    </div>
                    <% } %>
                    <button class="navbar-collapse-btn js-btn-collapse">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="navbar-nav-collapse-items js-navbar-nav-collapse p-3 mt">
                    <a class="btn btn-orange d-block btn-block" href="/">Pagina Principal</a>
                </div>
            </div>
        </div>
        <div class="container-fluid h-vh d-flex items-align-center justify-content-between">
            <div class="container">
                <div class="w-100 p-3 d-flex flex-column">
                    <div class="page-new-ad">
                        <h1 class="page-new-ad-header">Editar anuncio</h1>
                        <div>
                            <form action="/updateAd" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<%=ad.id%>">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="m-default">Localização do imóvel</h3>
                                        <span class="d-inline-block js-arrow arrow-up"></span>
                                    </div>
                                    <div class="js-card card-body text-center">
                                        <input class="w-70 mt-3 mr-3" name="street" type="text" placeholder="Endereço" value="<%=ad.street%>" required>
                                        <input class="w-20 mt-3 mr-3" name="number" type="text" placeholder="Nº" value="<%=ad.number%>" required>
                                        <input class="w-70 mt-3 mr-3" name="zone" type="text" placeholder="Bairro" value="<%=ad.zone%>" required>
                                        <input class="w-20 mt-3 mr-3" name="cep" type="text" placeholder="CEP" value="<%=ad.cep%>" required>
                                        <input class="w-70 mt-3 mr-3" name="city" type="text" placeholder="Cidade" value="<%=ad.city%>" required>
                                        <input class="my-state" type="hidden" value="<%=ad.state%>">
                                        <select class="w-20 mt-3 mr-3" name="state" required>
                                            <option class="js-state" value="Acre">AC</option>
                                            <option class="js-state" value="Alagoas">AL</option>
                                            <option class="js-state" value="Amapá">AP</option>
                                            <option class="js-state" value="Amazonas">AM</option>
                                            <option class="js-state" value="Bahia">BA</option>
                                            <option class="js-state" value="Ceará">CE</option>
                                            <option class="js-state" value="Distrito Federal">DF</option>
                                            <option class="js-state" value="Espírito Santo">ES</option>
                                            <option class="js-state" value="Goiás">GO</option>
                                            <option class="js-state" value="Maranhão">MA</option>
                                            <option class="js-state" value="Mato Grosso">MT</option>
                                            <option class="js-state" value="Mato Grosso do Sul">MS</option>
                                            <option class="js-state" value="Minas Gerais">MG</option>
                                            <option class="js-state" value="Pará">PA</option>
                                            <option class="js-state" value="Paraíba">PB</option>
                                            <option class="js-state" value="Paraná">PR</option>
                                            <option class="js-state" value="Pernambuco">PE</option>
                                            <option class="js-state" value="Piauí">PI</option>
                                            <option class="js-state" value="Rio de Janeiro">RJ</option>
                                            <option class="js-state" value="Rio Grande do Norte">RN</option>
                                            <option class="js-state" value="Rio Grande do Sul">RS</option>
                                            <option class="js-state" value="Rondônia">RO</option>
                                            <option class="js-state" value="Roraima">RR</option>
                                            <option class="js-state" value="Santa Catarina">SC</option>
                                            <option class="js-state" value="São Paulo">SP</option>
                                            <option class="js-state" value="Sergipe">SE</option>
                                            <option class="js-state" value="Tocantins">TO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="m-default">Caracteristicas do Imóvel</h3>
                                        <span class="d-inline-block arrow-down js-arrow"></span>
                                    </div>
                                    <div class="js-card display">
                                        <label for="property-type">
                                            <span class="card-span">Tipo do Imóvel :</span>
                                        </label>
                                        <br>
                                        <input class="my-type-immobile" type="hidden" value="<%=ad.typeImmobile%>">
                                        <select id="property-type" name="type" class="text-center mt-2" required>
                                            <option class="js-type-immobile" value="0">Apartamento</option>
                                            <option class="js-type-immobile" value="1">Casa</option>
                                            <option class="js-type-immobile" value="2">Ponto Comercial</option>
                                        </select>
                                        <br>
                                        <input class="mt-2" name="size" type="text" placeholder="Tamanho do imóvel M²" value="<%=ad.size%>" required>
                                        <br>
                                        <label for="bedroom">
                                            <span class="card-span">Nº Quartos</span>
                                        </label>
                                        <br>
                                        <input id="bedroom" name="bedroom" type="number" placeholder="0" class="mt-2" value="<%=ad.bedroom%>" required>
                                        <br>
                                        <label for="suite">
                                            <span class="card-span">Nº Suite</span>
                                        </label>
                                        <br>
                                        <input id="suite" name="suite" type="number" placeholder="0" class="mt-2" value="<%=ad.suite%>" required>
                                        <br>
                                        <label for="wc">
                                            <span class="card-span">Nº Banheiro</span>
                                        </label>
                                        <br>
                                        <input id="wc" name="wc" type="number" placeholder="0" class="mt-2" value="<%=ad.bathroom%>" required>
                                        <br>
                                        <label for="vacancy">
                                            <span class="card-span">Nº Vaga</span>
                                        </label>
                                        <br>
                                        <input id="vacancy" name="vacancy" type="number" placeholder="0" class="mt-2" value="<%=ad.vacancy%>" required>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="m-default">Negociação</h3>
                                        <span class="d-inline-block arrow-down js-arrow"></span>
                                    </div>
                                    <div class="js-card display">
                                        <label>
                                            <span class="card-span">Tipo de negociação</span>
                                            <br>
                                            <div class="js-sale btn bg-primary radio-select-1">Venda</div>
                                            <div class="js-rent btn bg-muted radio-select-1">Aluguel</div>
                                            <input class="js-trading-option" name="trading" type="hidden" value="<%=ad.tradingType%>" required>
                                        </label>
                                        <br>
                                        <input type="text" name="sale" placeholder="R$ - Valor da venda" value="<%=ad.price%>" class="mt-20" required>
                                        <br>
                                        <input type="text" name="condominium" placeholder="R$ - Condominio/Mês" value="<%=ad.condominium%>" class="mt-20" required>
                                        <br>
                                        <input type="text" name="iptu" placeholder="R$ - IPTU/Ano" value="<%=ad.iptu%>" class="mt-20" required>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="m-default">Titulo e descrição do anuncio</h3>
                                        <span class="d-inline-block arrow-down js-arrow"></span>
                                    </div>
                                    <div class="js-card display">
                                        <input type="text" name="title" placeholder="Titulo do anuncio" value="<%=ad.title%>" class="w-100 mt-20" required>
                                        <textarea name="description" class="w-100 h-225 mt-20 m-default" placeholder="Descrição do anuncio" required><%=ad.description%></textarea>
                                    </div>
                                    <div class="image-name">

                                    </div>
                                </div>
                                <input type="submit" class="submit d-none">
                            </form>

                            <form action="/imageUpload" method="POST" enctype="multipart/form-data">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="m-default">Fotos do imóvel</h3>
                                        <span class="d-inline-block arrow-down js-arrow"></span>
                                    </div>
                                    <div class="js-card display">
                                        <%for(int i = 0; i < ad.imageDate.size(); i++){%>
                                            <p class="d-none image-id"><%=ad.imageDate.get(i).getIdImage()%></p>
                                            <p class="d-none image-url"><%=ad.imageDate.get(i).getUrl()%></p>
                                        <%}%>
                                        <input id="fileupload" class="d-none files" type="file" name="files[]" data-url="/imageUpload" multiple>
                                        <div class="m-auto mb-20">
                                            <div class="btn btn-orange w-30 text-center mt-2 load d-inline-block">Carregar imagens</div>
                                        </div>
                                        <div class="m-auto text-center">
                                            <output id="list-images" class="list m-auto"></output>
                                        </div>
                                    </div>
                                </div>
                            </form>
                                <div class="mt-30 mb-4 text-right">
                                    <a class="btn btn-primary-outline" href="/myAds">Cancelar</a>
                                    <button class="btn btn-primary enter">Salvar Atualizações</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/vendor/jquery.ui.widget.js"></script>
        <script src="../../js/jquery.iframe-transport.js"></script>
        <script src="../../js/jquery.fileupload.js"></script>
        <script src="../../js/mobile.js"></script>
        <script src="../../js/selection.js"></script>
        <script src="../../js/control-edit.js"></script>
        <script src="../../js/verification-date.js"></script>
        <script src="../../js/handle.file.select.js"></script>
        <script src="../../js/menu.perfil.user.js"></script>
        <script src="../../js/preload-form.js"></script>
    </body>
</html>