FROM  mysql:5.7

COPY ./config/db/data/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
COPY ./config/db/tables/00_users.sql /docker-entrypoint-initdb.d/00_users.sql
COPY ./config/db/tables/01_types.sql /docker-entrypoint-initdb.d/01_types.sql
COPY ./config/db/tables/02_trading.sql /docker-entrypoint-initdb.d/02_trading.sql
COPY ./config/db/tables/03_adverts.sql /docker-entrypoint-initdb.d/03_adverts.sql
COPY ./config/db/tables/05_images.sql /docker-entrypoint-initdb.d/05_images.sql
COPY ./config/db/tables/05_images.sql /docker-entrypoint-initdb.d/05_images.sql
COPY ./config/db/tables/06_code_recovery.sql /docker-entrypoint-initdb.d/06_code_recovery.sql

RUN echo "CREATE USER 'home'@'%' IDENTIFIED BY 'home456';GRANT ALL PRIVILEGES ON *.* TO 'home'@'%' WITH GRANT OPTION;" > /docker-entrypoint-initdb.d/_grant_remote.sql

EXPOSE 3306

CMD ["mysqld"]