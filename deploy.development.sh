#!/bin/bash

# Empacotando aplicação...
mvn package

# "Buildando" imagens
docker build -t app -f app.dockerfile .
docker build -t db -f db.dockerfile .

# "Subindo" containers
docker-compose up -d --force-recreate