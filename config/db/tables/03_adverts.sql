CREATE TABLE adverts(
    id varchar(40) PRIMARY KEY NOT NULL,
    street varchar(40) NOT NULL,
    number  varchar(1000) NOT NULL,
    zone varchar(40) NOT NULL,
    cep varchar(40) NOT NULL,
    city varchar(40) NOT NULL,
    state varchar(40) NOT NULL,
    type_immobile varchar(9) NOT NULL,
    size varchar(1000) NOT NULL,
    bedroom varchar(20) NOT NULL,
    suite varchar(20) NOT NULL,
    bathroom varchar(20) NOT NULL,
    vacancy varchar(20) NOT NULL,
    trading_type varchar(9) NOT NULL,
    price varchar(10) NOT NULL,
    condominium varchar(10) NOT NULL,
    iptu varchar(10) NOT NULL,
    title varchar(80) NOT NULL,
    description LONGTEXT,
    date date NOT NULL,
    user_id varchar(40),
    CONSTRAINT  fk_user_id FOREIGN KEY (user_id) REFERENCES users(id),
    CONSTRAINT fk_types_immobile FOREIGN KEY (type_immobile) REFERENCES types(id),
    CONSTRAINT fk_trading_immobile FOREIGN KEY (trading_type) REFERENCES trading(id)
);

-- INSERT INTO adverts(id, street, number, zone, cep, city, state, type_immobile, size, bedroom, suite, bathroom, vacancy, trading_type, price, condominium, iptu, title, description, date) VALUES ("8037e842-0639-4550-821d-d168bd1bd91d","Rua agapito dos santos","435","Centro", "60180200", "Fortaleza", "Ceará", "1", "50", "2", "1", "1", "0", "2", "500,00", "00,00", "00,00" "Apartamento ventilado em todos os comodos", "Cor da casa amarela, tem varios comodos, paredes bem conservadas e possui garagem", "2019-12-10");
-- INSERT INTO adverts(id, title, street, number, zone, city, state, description, price, date) VALUES ("b6264af3-9cce-4601-93eb-144d4705e59d","Apartamento ventilado em todos os comodos","Rua agapito dos santos","435","Centro","Fortaleza","Ceara","Cor da casa amarela, tem varios comodos, paredes bem conservadas e possui garagem","900.000,00","2019-12-10");
-- INSERT INTO adverts(id, title, street, number, zone, city, state, description, price, date) VALUES ("eca5a701-b1e8-48fc-925a-b52bf889bf0b","Apartamento ventilado em todos os comodos","Rua agapito dos santos","435","Centro","Fortaleza","Ceara","Cor da casa amarela, tem varios comodos, paredes bem conservadas e possui garagem","800.000,00","2019-12-10");
-- INSERT INTO adverts(id, title, street, number, zone, city, state, description, price, date) VALUES ("aecdcb52-60dc-4803-bdef-7ef9ecedba56","Apartamento ventilado em todos os comodos","Rua agapito dos santos","435","Centro","Fortaleza","Ceara","Cor da casa amarela, tem varios comodos, paredes bem conservadas e possui garagem","600.000,00","2019-12-10");