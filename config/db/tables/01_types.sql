CREATE TABLE types (
    id varchar(9) NOT NULL,
    type varchar(30) NOT NULL,
    PRIMARY KEY (id)
);

-- INSERT INTO types(id, type) VALUES ("0", "Apartamento");
-- INSERT INTO types(id, type) VALUES ("1", "Casa");
-- INSERT INTO types(id, type) VALUES ("2", "Ponto Comercial");