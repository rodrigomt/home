CREATE TABLE code_recovery(
    id varchar(40) PRIMARY KEY NOT NULL,
    code varchar(5) NOT NULL,
    status varchar(1) NOT NULL,
    id_user varchar(40) NOT NULL
);