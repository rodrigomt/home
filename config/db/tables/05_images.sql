CREATE TABLE images (
    id varchar(40) PRIMARY KEY NOT NULL,
    url LONGTEXT NOT NULL,
    id_ad varchar(40),
    CONSTRAINT fk_imageImmobile FOREIGN KEY (id_ad) REFERENCES adverts(id)
);

-- INSERT INTO images (id, url, id_ad) VALUES ("2255b159-7659-4aff-ac87-3934efc8ae40", "../img/predio.jpg", "8037e842-0639-4550-821d-d168bd1bd91d");
-- INSERT INTO images (id, url, id_ad) VALUES ("f48e0c18-679e-405f-914d-0d69b323f933", "../img/predio02.jpg", "8037e842-0639-4550-821d-d168bd1bd91d");
-- INSERT INTO images (id, url, id_ad) VALUES ("6624dd67-7a21-43bb-8501-5d051f09f620", "../img/predio03.jpg", "8037e842-0639-4550-821d-d168bd1bd91d");
-- INSERT INTO images (id, url, id_ad) VALUES ("98b8826f-7364-4672-8494-7da299b00e24", "../img/predio04.jpg", "8037e842-0639-4550-821d-d168bd1bd91d");

-- INSERT INTO images (id, url, id_ad) VALUES ("d60ffc81-c377-477f-8f94-fb2811d0997a", "../img/predio05.jpg", "b6264af3-9cce-4601-93eb-144d4705e59d");
-- INSERT INTO images (id, url, id_ad) VALUES ("ddfef114-728d-4b8d-a803-56396b3cf37a", "../img/predio06.jpg", "b6264af3-9cce-4601-93eb-144d4705e59d");
-- INSERT INTO images (id, url, id_ad) VALUES ("3094072f-426f-4adb-bc8f-f6fe6ad5c079", "../img/predio07.jpg", "b6264af3-9cce-4601-93eb-144d4705e59d");
-- INSERT INTO images (id, url, id_ad) VALUES ("4a97c864-4096-4ce7-9ba6-b11e7e349a19", "../img/predio08.jpg", "b6264af3-9cce-4601-93eb-144d4705e59d");
--
-- INSERT INTO images (id, url, id_ad) VALUES ("6966d060-ce2b-4bb4-b506-6a218f4e1dc7", "../img/predio09.jpg", "eca5a701-b1e8-48fc-925a-b52bf889bf0b");
-- INSERT INTO images (id, url, id_ad) VALUES ("0d071ed2-74f6-4adc-97b3-39b456fbcbce", "../img/predio10.jpg", "eca5a701-b1e8-48fc-925a-b52bf889bf0b");
-- INSERT INTO images (id, url, id_ad) VALUES ("93919182-dfad-4a7b-81f9-32a5740244da", "../img/predio11.jpg", "eca5a701-b1e8-48fc-925a-b52bf889bf0b");
-- INSERT INTO images (id, url, id_ad) VALUES ("9aba38a6-8031-4aac-bd13-6e8b794bab54", "../img/predio12.jpg", "eca5a701-b1e8-48fc-925a-b52bf889bf0b");
--
-- INSERT INTO images (id, url, id_ad) VALUES ("824014be-bafe-4023-8c7b-3e866f813c1e", "../img/predio13.jpg", "aecdcb52-60dc-4803-bdef-7ef9ecedba56");
-- INSERT INTO images (id, url, id_ad) VALUES ("db18ba9d-460c-4681-9748-60c74e77efbf", "../img/predio14.jpg", "aecdcb52-60dc-4803-bdef-7ef9ecedba56");
-- INSERT INTO images (id, url, id_ad) VALUES ("3ffab90c-d1be-4513-8589-93b99bcb1d68", "../img/predio15.jpg", "aecdcb52-60dc-4803-bdef-7ef9ecedba56");
-- INSERT INTO images (id, url, id_ad) VALUES ("3e4cc9aa-589f-4af1-a15f-1f559c4eb085", "../img/predio16.jpg", "aecdcb52-60dc-4803-bdef-7ef9ecedba56");